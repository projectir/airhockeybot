# $ cd airhockeybot && pip install -r requirements.txt
pyyaml
rospkg
scikit-learn
numpy
opencv-python==3.4.3.18
empy
cython
defusedxml
PySide2
PyQt5
pydot
pynput
pyserial
