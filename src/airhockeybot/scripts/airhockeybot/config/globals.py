#!/usr/bin/env python3"""Container for global config class."""

import os
import json
from os.path import dirname

class GlobalConfiguration(dict):
    """Wrapper class for the globals config file.

    Args:
        path_to_json: Full path to the globals.json configuration file.
    """

    def __init__(self):
        dict.__init__(self)
        dir = dirname(dirname(dirname(dirname(dirname(dirname(os.path.abspath(__file__))))))) + "/globals.json"
        assert os.path.isfile(dir), "No valid file under '%s'." % dir
        with open(dir, "r") as file:
            json_dict = json.load(file)
        for key in json_dict:
            self[key] = json_dict[key]
        self.path_to_json = dir
