#!/usr/bin/env python3"""A configuration class for the project."""
from airhockeybot.config.globals import GlobalConfiguration


class ProjectConfiguration(dict):
    """Model for the configuration of this air hockey puck tracking project.

    This ProjectConfiguration includes the globals.json configuration.
    Further configurations, e.g. custom hyperparameter configurations,
    can be easily attached. This allows to use one coherent configuration
    format through out the whole project.

    Args:
        path_to_globals_json:   Full path to the globals.json configuration file.
    """

    def __init__(self):
        dict.__init__(self)
        self["globals"] = GlobalConfiguration()
