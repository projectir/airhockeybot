from distutils.core import setup

setup(
    version='0.0.0',
    scripts=['scripts'],
    packages=['airhockeybot'],
    package_dir={'': 'scripts'}
)
