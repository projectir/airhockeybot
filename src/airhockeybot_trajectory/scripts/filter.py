from abc import abstractmethod
import numpy as np
from trajectory import Trajectory
from observation import Observation

# Must inherit from 'object' 
# to be new style class


class Filter:
    """
    Filter to integrate puck position measurements for AirhockeyBot's ROS pipeline in airhockeybot_image_infomration_processor.py
    """

    trajectories = None

    def __init__(self, filter_size=5):
        self.trajectories = np.empty(shape=filter_size, dtype=Trajectory)

    @abstractmethod
    def update(self, observation: Observation):
        raise NotImplementedError()
