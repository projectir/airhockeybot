#!/usr/bin/env python3
"""Filter that generates trajectories from observations."""

import copy
import operator
import itertools
import collections

import rospy
import numpy
from sklearn import linear_model

from filter import Filter
from trajectory import Trajectory
from observation import Observation
from intersection import Intersection
from airhockeybot.config.project import ProjectConfiguration

DEBUG = False

LEFT = 0
RIGHT = 1
TOP = 2
BOTTOM = 3


class SliceableDeque(collections.deque):
    """Deque variant which can get sliced (e.g. [5:8]) and get last n items.

    https://stackoverflow.com/a/10003201/8627992
    """
    def __getitem__(self, index):
        if isinstance(index, slice):
            return type(self)(itertools.islice(self, index.start,
                                               index.stop, index.step))
        return collections.deque.__getitem__(self, index)

    def last_n(self, amount):
        """Retrieve the last n items from the deque."""
        item_count = len(self)
        rightmost = self[max((item_count - amount, 0)):item_count]
        return rightmost


class MaximumLikelihoodFilter(Filter):
    """Trajectory Generator. See Readme for theoretical background.

    Incorporates a sliding window over the last n observations and resets
    previous observations after a change of direction.
    """

    config = ProjectConfiguration()
    border_proximity_tolerance = config["globals"]["trajectory"]["border_proximity_tolerance"]
    queue_length = config["globals"]["trajectory"]["queue_length"]

    def __init__(self, filter_size=5):
        super(MaximumLikelihoodFilter, self).__init__(filter_size)
        self.history = SliceableDeque(maxlen=self.queue_length)

    def update(self, observation):
        """Register a new observation and update the trajectories."""
        if not self.valid_observation(observation):
            return
        self.history.append(observation)
        if len(self.history) <= 1:  # At least 2 observations required.
            return

        if self.minimal_movement():  # Check if puck moves
            self.clear_trajectories()
            return self.trajectories

        # Check correlation between last and new trajectory estimate.
        self.prune_history()

        # Set DEBUG to true to offline-evaluate friction on your table
        if DEBUG:
            self.test_friction()

        new = self.trajectory_from_history(self.history)
        self.trajectories[0] = new
        for index in range(1, len(self.trajectories)):
            followup = self.trajectory_from_trajectory(self.trajectories[index - 1])
            self.trajectories[index] = followup
        return self.trajectories

    def test_friction(self):
        """Find the rate at which the puck loses speed.

        This is dependant on table and puck, result can then be saved as
        constant.
        """
        diffs = []
        if len(self.history) <= 20:
            return
        previous = None
        for item in self.history.last_n(20):
            if previous is None:
                previous = item
                continue
            # noinspection PyTypeChecker
            space_diff = point_distance(previous, item)
            time_diff = abs(previous.t - item.t)
            diffs.append((space_diff, time_diff))
            previous = item

        if abs(correlation(self.history.last_n(10))) > 0.95:
            data = ["Diagram:"]
            running_t = 0
            running_s = 0
            times = []
            value = []
            for diff in diffs:
                data.append("{} {} {} {}".format(diff[1], running_t,
                                                 diff[0], running_s))
                times.append(running_t)
                value.append(diff[0])
                running_t += diff[1]
                running_s += diff[0]
            # rospy.loginfo("\n".join(data).replace(".", ","))
            x_reg = linear_model.LinearRegression()
            x_reg.fit(numpy.reshape(times, newshape=(-1, 1)), value)
            rospy.loginfo("Friction coefficient: %s", x_reg.coef_[0])
        # distance_difference = 1 - (max(travelled_distance, absolute_distance) /
        #                            min(travelled_distance, absolute_distance))

    def trajectory_from_history(self, observations):
        """Calculate current trajectory from recent observations."""
        x_batch = [obs.x for obs in observations]
        y_batch = [obs.y for obs in observations]
        t_batch = [obs.t for obs in observations]

        x_reg = linear_model.LinearRegression()
        y_reg = linear_model.LinearRegression()
        x_reg.fit(numpy.reshape(t_batch, newshape=(-1, 1)), x_batch)
        y_reg.fit(numpy.reshape(t_batch, newshape=(-1, 1)), y_batch)

        # New Trajectory starts from most recent observation, thus always
        # on the puck, not besides it, and inside the playing field
        new = Trajectory(
            observations[-1].x,
            x_reg.coef_[0],
            observations[-1].y,
            y_reg.coef_[0],
            observations,
        )
        if DEBUG and ((new.x_init < 0.001 and new.x_vel < 0) or
                      (new.y_init < 0.001 and new.y_vel < 0)):
            rospy.logerr("This trajectory is weird!!! %r", new)

        new.intersection = self.predict_intersection(new)
        return new

    def predict_intersection(self, trajectory):
        """Calculate an intersection for a trajectory."""
        radius = self.config["globals"]["puck"]["radius"]
        width = self.config["globals"]["playing_field"]["width"]
        height = self.config["globals"]["playing_field"]["height"]
        if trajectory.x_vel == 0 and trajectory.y_vel == 0:
            # TODO: This could probably be solved better than adding
            #   an imaginary point
            return Intersection(
                trajectory.x_init, trajectory.y_init, 0, True)

        # This contains the list of times it takes the puck on its current
        # trajectory to hit the walls of the playing field.
        intersection_times = [
            (LEFT  , ((         radius - trajectory.x_init) / trajectory.x_vel)),
            (RIGHT , ((width  - radius - trajectory.x_init) / trajectory.x_vel)),
            (TOP   , ((         radius - trajectory.y_init) / trajectory.y_vel)),
            (BOTTOM, ((height - radius - trajectory.y_init) / trajectory.y_vel)),
        ]

        intersection, selection = most_likely_intersection(trajectory, intersection_times)
        # Assuming float precision errors, the intersection might be around 1e-13
        # next to the initial point, if that happens, remove hypothetical
        # intersection and try again.
        if point_distance(intersection, Observation(trajectory.x_init, trajectory.y_init, 0)) < 0.01:
            intersection_times.remove(selection)
            intersection, _ = most_likely_intersection(trajectory, intersection_times)
        if DEBUG and not self.valid_observation(intersection, 100):
            rospy.loginfo("-" * 80)
            rospy.loginfo("Intersection point was outside of field!")
            rospy.loginfo("Intersection times: %s", intersection_times)
            rospy.loginfo("Start    : %10.4f, %10.4f", trajectory.x_init, trajectory.y_init)
            rospy.loginfo("Direction: %10.4f, %10.4f", trajectory.x_vel, trajectory.y_vel)
            rospy.loginfo("End      : %10.4f, %10.4f", intersection.x, intersection.y)
            # maybe raise exception here
            pass
        return intersection

    def trajectory_from_trajectory(self, previous):
        """From the preceding trajectory, calculate the next. In this model, outgoing angle = incoming angle at the
        point of intersection"""
        history = copy.deepcopy(previous.history)
        prev_intersection = Observation(previous.intersection.x,
                                        previous.intersection.y,
                                        previous.intersection.t)
        history.append(prev_intersection)
        next_trajectory = Trajectory(prev_intersection.x, previous.x_vel,
                                     prev_intersection.y, previous.y_vel,
                                     history)
        if previous.intersection.x_intersection: # mirror direction on long side
            next_trajectory.y_vel *= -1
        else: # mirror direction on short side
            next_trajectory.x_vel *= -1
        next_trajectory.intersection = self.predict_intersection(next_trajectory)
        if DEBUG and (   (next_trajectory.x_init < 0.001 and next_trajectory.x_vel < 0)
                      or (next_trajectory.y_init < 0.001 and next_trajectory.y_vel < 0)):
            rospy.logerr("This trajectory is weird!!! %r", next_trajectory)
            rospy.logerr("PreviousS: %10.4f, %10.4f", previous.x_init, previous.y_init)
            rospy.logerr("PreviousD: %10.4f, %10.4f", previous.x_vel, previous.y_vel)
            rospy.logerr("PreviousI: %10.4f, %10.4f", previous.intersection.x, previous.intersection.y)
            rospy.logerr("PreviousF: %s-intersection", "x" if previous.intersection.x_intersection else "y")
            rospy.logerr("Start    : %10.4f, %10.4f", next_trajectory.x_init, next_trajectory.y_init)
            rospy.logerr("Direction: %10.4f, %10.4f", next_trajectory.x_vel, next_trajectory.y_vel)
            rospy.logerr("Intersect: %10.4f, %10.4f", next_trajectory.intersection.x, next_trajectory.intersection.y)
        return next_trajectory

    def minimal_movement(self):
        """Check if the observations are very close to each other."""
        previous = None
        travelled_distance = 0
        for item in self.history:
            if previous is None:
                previous = item
                continue
            # noinspection PyTypeChecker
            travelled_distance += point_distance(previous, item)
            previous = item
        return travelled_distance < 4 * len(self.history)

    def valid_observation(self, observation, additional_tolerance=0):
        """Whether an observation within the boundaries of the field."""
        tolerance = self.border_proximity_tolerance + additional_tolerance
        width = self.config["globals"]["playing_field"]["width"]
        height = self.config["globals"]["playing_field"]["height"]
        x_is_valid = -tolerance <= observation.x <= width + tolerance
        y_is_valid = -tolerance <= observation.y <= height + tolerance
        return x_is_valid and y_is_valid

    def prune_history(self):
        """If the direction of the puck has changed, truncate history.

        Based on the correlation coefficient of the recent observations and
        the recently travelled distance. Checks correlation of last 5 items
        so that the truncation is fast and not multiple frames behind.
        """
        if len(self.history) <= 5:  # Give it some time to rebuild the history
            return
        corr = abs(correlation(self.history.last_n(5)))
        travelled_distance = 0
        previous = None
        for item in self.history:
            if previous is None:
                previous = item
                continue
            # noinspection PyTypeChecker
            travelled_distance += point_distance(previous, item)
            previous = item
        absolute_distance = point_distance(self.history[0], self.history[-1])
        if min(travelled_distance, absolute_distance) != 0:
            distance_difference = 1 - (max(travelled_distance, absolute_distance) /
                                       min(travelled_distance, absolute_distance))
        else:
            distance_difference = 0

        config = self.config["globals"]["trajectory"]
        pixel_per_frame = config["pixels_per_frame_threshold"]
        travel_distance_ratio = config["travel_distance_max_ratio"]
        min_correlation = config["min_correlation"]

        # Allow a cut if travelled distance is large enough
        # (flickering on slow movement)
        # OR if the total travelled distance and detailed travelled distance
        # diverge by a lot -> movement was not in a straight line
        distance_anomaly = (distance_difference > travel_distance_ratio
                            or travelled_distance > self.queue_length * pixel_per_frame)

        if corr <= min_correlation and distance_anomaly:
            self.truncate_history(remaining=2)
            # rospy.loginfo("Cutting History! c=%9.4f, dst=%9.4f, diff=%9.4f",
            #               corr, travelled_distance, distance_difference)

    def truncate_history(self, remaining=2):
        """Remove all but the last remaining items from the queue."""
        assert remaining >= 2, "History with less than 2 items: Bad idea"
        for _ in range(len(self.history) - remaining):
            self.history.popleft()

    def clear_trajectories(self):
        """Delete all previously calculated trajectories."""
        for index in range(len(self.trajectories)):
            self.trajectories[index] = None


def calculate_variance(observations):
    """Calculate variance of a history."""
    x_avg = 0
    y_avg = 0
    number_of_observations = len(observations)
    for observation in observations:
        x_avg += observation.x
        y_avg += observation.y
    x_avg = x_avg / number_of_observations
    y_avg = y_avg / number_of_observations
    x_error = 0
    y_error = 0
    for observation in observations:
        x_error += (observation.x - x_avg)**2
        y_error += (observation.y - y_avg)**2
    x_error = x_error / number_of_observations
    y_error = y_error / number_of_observations
    return x_error + y_error


def point_distance(intersection1, intersection2):
    """Calculate euclidean distance between two points with x/y attribute."""
    x_diff = intersection1.x - intersection2.x
    y_diff = intersection1.y - intersection2.y
    return numpy.linalg.norm((x_diff, y_diff))


def most_likely_intersection(trajectory, checklist):
    """From the list of intersection times, get the first positive one.

    Negative intersection time would mean the puck would have to move backwards,
    the selected one is then the first border line that is crossed.
    """
    intersection_times = [time for time in checklist if time[1] > 0]
    selection = min(intersection_times, key=operator.itemgetter(1))
    direction, intersection_time = selection

    x_intersection = direction in (TOP, BOTTOM)

    intersection = Intersection(
        (trajectory.x_init + (trajectory.x_vel * intersection_time)),
        (trajectory.y_init + (trajectory.y_vel * intersection_time)),
        intersection_time, x_intersection)
    return intersection, selection


def correlation(history):
    """Calculate the correlation of a list of 2d points."""
    series_x = [item.x for item in history]
    series_y = [item.y for item in history]
    return numpy.corrcoef(series_x, series_y)[0, 1]

