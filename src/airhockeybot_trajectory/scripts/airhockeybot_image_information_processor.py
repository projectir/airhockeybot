#!/usr/bin/env python3
# license removed for brevity

from timeit import default_timer as timer

import numpy
import rospy

from observation import Observation
from maximum_likelihood_filter import MaximumLikelihoodFilter
from airhockeybot.config.project import ProjectConfiguration

from airhockeybot_msgs.msg import TrajectoryInformation
from airhockeybot_msgs.msg import ImageInformation
from airhockeybot_msgs.msg import TransformationMatrix


class ImageProcessor:
    """ROS Runnable for the airhockeybot_trajectory package. The node holds an instance of MaximumLikelihoodFilter from
    maximum_likelihood_filter.py. After a new puck position arrived, update() method on the filter is called. For
    further information see the paper linked in the Readme. \n
    Subscribes to:
        /airhockeybot_image_information
        /airhockeybot_marker_information \n
    Publishes to:
        /airhockeybot_trajectory_information
    """

    config = ProjectConfiguration()

    def __init__(self):
        rospy.init_node('airhockeybot_image_information_processor', anonymous=True)

        trajectory_length = self.config["globals"]["trajectory"]["number_trajectories"]
        self.filter = MaximumLikelihoodFilter(trajectory_length)
        self.transformation_to_world = None
        self.transformation_to_image = None
        self.processing_times = []

        self.publisher = rospy.Publisher('airhockeybot_trajectory_information',
                                         TrajectoryInformation, queue_size=10)
        rospy.Subscriber("airhockeybot_image_information",
                         ImageInformation, self.process_image)
        rospy.Subscriber("airhockeybot_marker_information",
                         TransformationMatrix, self.update_transformations)
        rospy.spin()

    def update_transformations(self, data):
        """Save transformation matrices from /airhockeybot_marker_information topic to local variable."""
        transformation = numpy.array(data.to_world).reshape((2, 3))
        self.transformation_to_world = transformation.astype(numpy.float32)
        transformation = numpy.array(data.to_image).reshape((2, 3))
        self.transformation_to_image = transformation.astype(numpy.float32)
        # rospy.loginfo("[TransformationMatrix] \n" + str(data))

    def image_to_world(self, data):
        """Convert image position to world position."""
        world_coordinate = numpy.matmul(
            numpy.array([data.puck_position.x, data.puck_position.y, 1]),
            self.transformation_to_world.transpose())
        return world_coordinate[0], world_coordinate[1]

    def process_image(self, data):
        """Process puck position measurement. update() method on the maximum likelihood filter is called with the
        new observation. Additionally, statistics on the computation time are monitored"""
        if self.transformation_to_world is None:
            return
        start = timer()

        # Transform coordinates and update filter
        world_coordinate = self.image_to_world(data)
        observation = Observation(*world_coordinate, data.puck_position.t)
        update = self.filter.update(observation)

        # Publish trajectory
        if update is None:
            return
        msg = TrajectoryInformation()
        msg.message = "Test"
        msg.trajectories = [trajectory.message for trajectory in update if trajectory]
        self.publisher.publish(msg)
        end = timer()

        # Runtime monitoring
        self.processing_times.append(end - start)
        if len(self.processing_times) % 1000 == 0 and len(self.processing_times) != 0:
            avg_time = sum(self.processing_times) / len(self.processing_times)
            rospy.loginfo("[TRAJECTORY ESTIMATION]    Computation time in s, avg over 1000 frames: %s", avg_time)
            self.processing_times.clear()  # empty list


if __name__ == '__main__':
    try:
        ImageProcessor()
    except rospy.ROSInterruptException:
        pass