"""Observation data class."""

from airhockeybot_msgs.msg import PuckPosition as PuckPositionMessage


class Observation:
    """Observation data class."""

    def __init__(self, x, y, t):
        self.x = x
        self.y = y
        self.t = t

    def __str__(self):
        template = "Observation: X={}, Y={}, T={}"
        return template.format(self.x, self.y, self.t)

    def __repr__(self):
        template = "Observation({!r}, {!r}, {!r})"
        return template.format(self.x, self.y, self.t)

    def __eq__(self, other):
        if not isinstance(other, Observation):
            return False
        return (self.x == other.x and
                self.y == other.y and
                self.t == other.t)

    @property
    def message(self):
        """Convert current object to a ros message."""
        observation = PuckPositionMessage()
        observation.x = self.x
        observation.y = self.y
        observation.t = self.t
        return observation
