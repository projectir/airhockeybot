"""Trajectory data class."""

from airhockeybot_msgs.msg import Trajectory as TrajectoryMessage


class Trajectory:
    """Trajectory data class."""

    def __init__(self, x_init, x_vel, y_init, y_vel, history, intersection=None):
        self.x_init = x_init
        self.x_vel = x_vel
        self.y_init = y_init
        self.y_vel = y_vel
        self.history = history
        self.intersection = intersection

    def __str__(self):
        template = ("Trajectory: X_0={}, X_velocity={}, Y_0={}, Y_velocity={}\n"
                    "^---------: Intersection: {}, History: {}")
        return template.format(
            self.x_init, self.x_vel,
            self.y_init, self.y_vel,
            self.intersection, self.history)

    def __repr__(self):
        template = "Trajectory({!r}, {!r}, {!r}, {!r}, {!r}, {!r})"
        return template.format(self.x_init, self.x_vel,
                               self.y_init, self.y_vel,
                               self.history, self.intersection)

    def __eq__(self, other):
        if not isinstance(other, Trajectory):
            return False
        return (self.x_init == other.x_init and
                self.y_init == other.y_init and
                self.x_vel == other.x_vel and
                self.y_vel == other.y_vel and
                self.intersection == other.intersection)

    @property
    def message(self):
        """Convert current object to a ros message."""
        trj = TrajectoryMessage()
        trj.x_init = self.x_init
        trj.x_vel = self.x_vel
        trj.y_init = self.y_init
        trj.y_vel = self.y_vel
        trj.observations = [observation.message for observation in self.history]
        trj.intersection = self.intersection.message
        return trj
