"""Intersection data class."""

from airhockeybot_msgs.msg import Intersection as IntersectionMessage


class Intersection:
    """Intersection data class."""

    def __init__(self, x, y, t, x_intersection):
        self.x = x
        self.y = y
        self.t = t
        self.x_intersection = x_intersection

    def __str__(self):
        template = "Intersection: X={} Y={} T={} x_intersection={}"
        return template.format(self.x, self.y, self.t, self.x_intersection)

    def __repr__(self):
        template = "Intersection({!r}, {!r}, {!r}, {!r})"
        return template.format(self.x, self.y, self.t, self.x_intersection)

    def __eq__(self, other):
        if not isinstance(other, Intersection):
            return False
        return (self.x == other.x and
                self.y == other.y and
                self.x_intersection == other.x_intersection and
                self.t == other.t)

    @property
    def message(self):
        """Convert current object to a ros message."""
        intersection = IntersectionMessage()
        intersection.x = self.x
        intersection.y = self.y
        intersection.t = self.t
        return intersection
