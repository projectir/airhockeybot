# airhockeybot_trajectory package

## Overview

This package is dedicated to integrate puck location data received from AirhockeyBot's [vision module](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_vision) into a puck trajectory estimate.

![Drag Racing](./doc/img/trajectory_estimate.png)

**Keywords:** Trajectory Estimation, Maximum Likelihood Filter, Data Processing 

### Credentials

**Author: Jan Synwoldt, Jan-Gerrit Habekost, Marcus Rottschäfer<br />
Affiliation: [Masterproject 20/21 @ Technical Aspects of Multimodal Systems (TAMS), Department of Informatics, University of Hamburg](https://tams.informatik.uni-hamburg.de/) <br/>
Maintainer: Jan Synwoldt, 4synwold@informatik.uni-hamburg.de<br />
Jan-Gerrit Habekost, 9habekos@informatik.uni-hamburg.de**

The airhockeybot_trajectory package has been tested under [ROS] Melodic on respectively Ubuntu 18.04 and 20.04.
This is research code, expect that it changes often and any fitness for a particular purpose is disclaimed.


### Publications

If you use this work in an academic context, please cite the following publication:

* J.-G. Habekost: **Real-time Trajectory Estimation for Fast Moving Objects**. 2020. ([PDF](https://gitlab.com/projectir/airhockeybot/-/blob/master/docs/pdf/Habekost_2020_Real-time_Trajectory_Estimation_For_Fast_Moving_Objects.pdf))

        @techreport{Habekost2020,
            address = {Hamburg},
            author = {Habekost, Jan-Gerrit},
            institution = {Masterproject Intelligent Robotics 20/21, Technical Aspects of Multimodal Systems (TAMS), Depatment of Informatics, University of Hamburg},
            url = {https://gitlab.com/projectir/airhockeybot/-/blob/develop/docs/pdf/Habekost{\_}2020{\_}Real-time{\_}Trajectory{\_}Estimation{\_}For{\_}Fast{\_}Moving{\_}Objects.pdf},
            pages = {6},
            title = {{Real-time Trajectory Estimation for Fast Moving Objects}},
            year = {2020}
        }

### Dependencies

#### Third-party packages
- [Robot Operating System (ROS)](http://wiki.ros.org) (middleware for robotics),
- [SciKit  Learn](https://scikit-learn.org/stable/) (machine learning library)
- [Numpy](https://scikit-learn.org/stable/) (linear algebra library)

#### AirhockeyBot packages
- [airhockeybot_msgs](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_msgs) (custom ROS communication datatypes)
- [airhockeybot](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot) (global config package)   


## Usage

To start up the whole AirhockeyBot system follow the [project's ReadMe](https://gitlab.com/projectir/airhockeybot/-/tree/master). 
The airhockeybot_trajectory package can also be executed independently by following the instructions below.

#### Launch files
Run the main node with

	roslaunch airhockeybot_trajectory trajectory.launch

#### Config files

This project maintains all static values of the airhockey table, as well as system constants in [globals.json](https://gitlab.com/projectir/airhockeybot/-/blob/master/globals.json). Note that the file has to be correctly adapted to your custom setup, in order to make the `airhockeybot_trajectory` package work. 

#### Development

The main classes in this package are `ImageProcessor` ([see docs](https://projectir.gitlab.io/airhockeybot/classscripts_1_1airhockeybot__image__information__processor_1_1ImageProcessor.html))
and `MaximumLikelihoodFilter` ([see docs](https://projectir.gitlab.io/airhockeybot/classscripts_1_1maximum__likelihood__filter_1_1MaximumLikelihoodFilter.html)).


## Nodes

### /airhockeybot_image_information_processor

Subscribes to `puck location` and `coordinate transformation` topic and integrates positional observations into a trajectory.


#### Subscribed Topics

* **`/airhockeybot_image_information`** ([airhockeybot_msgs/ImageInformation](https://gitlab.com/projectir/airhockeybot/-/blob/master/src/airhockeybot_msgs/msg/ImageInformation.msg))

	Puck position, determined by the vision module.
	
* **`/airhockeybot_marker_information`** ([airhockeybot_msgs/TransformationMatrix](https://gitlab.com/projectir/airhockeybot/-/blob/master/src/airhockeybot_msgs/msg/TransformationMatrix.msg))

	Transformation matrix (image plane <--> world coordinate system), determined by the vision module.


#### Published Topics

* **`/airhockeybot_trajectory_information`** ([airhockeybot_msgs/TrajectoryInformation](https://gitlab.com/projectir/airhockeybot/-/blob/master/src/airhockeybot_msgs/msg/TrajectoryInformation.msg))

	Trajectory estimate containing the `current trajectory`, a `set of future trajectories`, the `intersection point` with the 
	table's wall and `all observations` belonging to the current trajectory.


#### Parameters
All parameters can be found in [globals.json](https://gitlab.com/projectir/airhockeybot/-/blob/master/globals.json).

###### Playing Field
* **`width`** (float, default: 1802.0, unit: mm)
* **`height`** (float, default: 884.0, unit: mm)

    Defines the size of the playing field.
    
###### Trajectory Prediction
* **`number_trajectories`** (int, default: 4)

    Defines the number of future puck trajectories tha are predicted during runtime.

* **`queue_length`** (int, default: 60)

    Defines the max. number of observations that are integrated into a trajectory.

* **`border_proximity_tolerance`** (int, default: -20, unit: pixels)

    Tolerance that prevents erroneous measurements, that lie out of the playing field, from being integrated into the trajectory. There should be no reason to change this value.

* **`pixels_per_frame_threshold`** (int, default: 4, unit: pixels)

    In case the difference between the `last puck position` and the `current position` is lower than the `pixels_per_frame_threshold`, 
    the new measurement is not integrated and we dont provide a trajectory. This is due to the fact, that the table top of our airhockey table is not 
    perfectly flat and following from this our puck does not behave linear at extremely low speeds. If your table does 
    not show this problem set the value to 100 or higher, for our table the threshold is 4.

* **`travel_distance_max_ratio`** (float, default: 0.3)

    Threshold to detect high distances between the `last observation` and the `current observation`, which indicates an error, e.g. a temporary malfunction of the vision module.  There should be no reason to change this value. 

* **`min_correlation`** (float, default: 0.98)

    If the correlation between the `prior trajectory` and the `new trajectory estimate` is lower than this threshold, 
    `trajectory manipulation`, e.g. through the opponent player, is detected. 
    Therefore the observation history is cleaned and the trajectory is recalculated from the last known puck position 
    and the next measurement that arrives.
