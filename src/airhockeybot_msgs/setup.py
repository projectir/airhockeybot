from distutils.core import setup

setup(
    version='0.0.0',
    packages=['airhockeybot_msgs'],
    package_dir={'': 'src'}
)
