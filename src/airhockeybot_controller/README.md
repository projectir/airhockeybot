# airhockeybot_controller package

## Overview

This package is dedicated to interface robot coordinates received from AirhockeyBot's [action module](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_action) and the [simulation module](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_simulation) or rather the embedded controller.


### Credentials

**Author: Nicolas Frick, Lasse Haffke, Mirko Hartung, Jan-Tarek Butt, Jan Synwoldt, Jan-Gerrit Habekost, Marcus Rottschäfer<br />
Affiliation: [Masterproject 20/21 @ Technical Aspects of Multimodal Systems (TAMS), Department of Informatics, University of Hamburg](https://tams.informatik.uni-hamburg.de/) <br/>
Maintainer:  Nicolas Frick, 9frick@informatik.uni-hamburg.de**

The airhockeybot_controller package has been tested under [ROS] Melodic on respectively Ubuntu 18.04 and 20.04.
This is research code, expect that it changes often and any fitness for a particular purpose is disclaimed.

### Dependencies

#### Third-party packages
- [Robot Operating System (ROS)](http://wiki.ros.org) (middleware for robotics),
- [Pynput](https://pypi.org/project/pynput/) (keyboard logger library)
- [Numpy](https://scikit-learn.org/stable/) (linear algebra library)

#### AirhockeyBot packages
- [airhockeybot_msgs](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_msgs) (custom ROS communication datatypes)
- [airhockeybot](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot) (global config package)   


## Usage

To start up the whole AirhockeyBot system follow the [project's ReadMe](https://gitlab.com/projectir/airhockeybot/-/tree/master). 
The airhockeybot_controller package can also be executed independently by following the instructions below.

#### Launch files
Run the main node with

	roslaunch airhockeybot_controller airhockeybot_controller.launch

#### Config files

This project maintains all static values of the airhockey table, as well as system constants in [globals.json](https://gitlab.com/projectir/airhockeybot/-/blob/master/globals.json). Note that the file has to be correctly adapted to your custom setup, in order to make the `airhockeybot_controller` package work. 

#### Development

The main class in this package is `AirhockeybotController` ([see docs](https://projectir.gitlab.io/airhockeybot/group__airhockeybot__controller.html)).


## Nodes

### /airhockeybot_controller

Subscribes to `blocking point` receiving xy coordinates for the endeffector to block the puck at. The coordinates are published either to `gazebo` or to the `embedded controller`.


#### Subscribed Topics

* **`/blocking_point`** ([airhockeybot_msgs/BlockingPoint](https://gitlab.com/projectir/airhockeybot/-/blob/master/src/airhockeybot_msgs/msg/BlockingPoint.msg))

	Puck blocking coordinates, determined by the action module.

* **`/airhockeybot/joint_states`** ([JointState](http://docs.ros.org/en/melodic/api/sensor_msgs/html/msg/JointState.html))

    Joint position values for the robot in the simulation (not implemented for the hardware).

* **`/gazebo/link_states`** ([LinkStates](http://docs.ros.org/en/groovy/api/gazebo/html/msg/LinkStates.html))

    Position values for the puck in the simulation.


#### Published Topics

* **`/joint_x_position/command`** ([gazebo_ros_control](http://gazebosim.org/tutorials/?tut=ros_control))
* **`/joint_y_position/command`** ([gazebo_ros_control](http://gazebosim.org/tutorials/?tut=ros_control))

Joint position commands for the endeffector in simulation.


* **`/player_joint_x_position/command`** ([gazebo_ros_control](http://gazebosim.org/tutorials/?tut=ros_control))
* **`/player_joint_y_position/command`** ([gazebo_ros_control](http://gazebosim.org/tutorials/?tut=ros_control))

Joint position commands for the player stick in simulation.

The `joint_position/command` topic is part of the `gazebo_ros_control` plugin. For more information, refer to [Airhockeybot simulation](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_simulation).

#### Parameters
All parameters can be found in [globals.json](https://gitlab.com/projectir/airhockeybot/-/blob/master/globals.json).

###### ROS control frequency
* **`F_CTRL_LOOP`** (int, default: 100, unit: Hz)

    Defines the main control loop frequency.
    
###### Robot control

* **`MOVE`** (float, default: 0.2, unit: m)

    Defines the stepwidth the robot moves in simulation.

* **`FACTOR`** (int, default: 8, unit: -)

    Defines the factor the puck stepwidth is multiplied with in simulation.

* **`COMMANDS`** (dict, -, -)

    Defines a dictionary mapping user input to actions. Used to remote control the robot via keyboard.
