#!/bin/python
"""Computer!"""

import os.path
import threading
import time
import tkinter

import serial
import inputs


EVENT = threading.Event()


class FakeRobot():
    """Robot interface."""

    def __init__(self):
        """Initialize communication."""
        print("Ready")
        self.serial = None
        self.current_position = (0, 0)

    def periodic_move(self, field):
        """Send robot to current position."""
        print("Printer started!")
        while not EVENT.is_set():
            time.sleep(1)
            # sendcommand(self.serial, "G80")  # Cancel current motion
            self.move(field.robotspace, 25000)

    def cancel_move(self):
        """Cancel the current movement. In Progress."""
        # sendcommand(self.serial, "G80")  # Cancel current motion

    def move(self, pos, speed=5000):
        """Write data to board."""
        message = "G0 X{pos[1]} Y{pos[1]} Z{pos[0]} F{speed}".format(pos=pos, speed=speed)
        print(message)
        # sendcommand(self.serial, message, waittime=0)
        self.current_position = pos
        # print("Position: {0[0]:>6} {0[1]:>6}".format(pos_glob), end="\r")
        # # x: minus ist links
        # # y: minus ist oben

class Robot():
    """Robot interface."""

    def findSerialPort(self):
        """Returns the path to a serial port as a string. The port with the name /dev/ttyUSBX only exists if a mikrocontroller is attached"""
        basename = "/dev/ttyUSB"

        for n in range(10):
            fname = basename + str(n)
            if os.stat(fname):
                return fname
            return "/dev/null"

    def __init__(self):
        """Initialize communication."""
        ser = serial.Serial(self.findSerialPort(), 250_000)
        waittime = 0.1
        time.sleep(waittime)
        sendcommand(ser, "M906 X1500 Y1500 Z1500")
        sendcommand(ser, "M92 X-40 Y40 Z40")
        sendcommand(ser, "M203 X10000 Y10000 Z10000")
        sendcommand(ser, "G92 X0 Y0 Z0")
        # sendcommand(ser, "M122 S1\n")  # Debug
        sendcommand(ser, "M201 X100 Y100 Z100")
        sendcommand(ser, "M204 T500")  # Acceleration travel speed
        # sendcommand(ser, "G80")  # Cancel current motion
        print("Ready")
        self.serial = ser
        self.current_position = (0, 0)

    def periodic_move(self, field):
        """Send robot to current position."""
        print("Printer started!")
        while not EVENT.is_set():
            time.sleep(1)
            # sendcommand(self.serial, "G80")  # Cancel current motion
            self.move(field.robotspace, 10000)

    def cancel_move(self):
        """Cancel the current movement. In Progress."""
        sendcommand(self.serial, "G80")  # Cancel current motion

    def move(self, pos, speed=5000):
        """Write data to board."""
        message = "G0 X{pos[1]} Y{pos[1]} Z{pos[0]} F{speed}".format(pos=pos, speed=speed)
        print(message)
        sendcommand(self.serial, message, waittime=0)
        self.current_position = pos
        # print("Position: {0[0]:>6} {0[1]:>6}".format(pos_glob), end="\r")
        # # x: minus ist links
        # # y: minus ist oben


class ControllerPos():
    """Current controller position."""

    def __init__(self, gamepad):
        self.x = 0  # pylint: disable=invalid-name
        self.y = 0  # pylint: disable=invalid-name
        self.gamepad = gamepad
        self.name = gamepad.name
        self.buttonfunc = None
        self.field = None

    @property
    def position(self):
        """Position as tuple."""
        return self.x, self.y

    def update(self, event):
        """Update controller input."""
        if event.code in ("ABS_Y", "ABS_X"):
            if event.code == "ABS_X":
                self.x = event.state
            elif event.code == "ABS_Y":
                self.y = event.state
        elif event.code == "BTN_TR" and event.state == 1:
            if callable(self.buttonfunc) and self.field is not None:
                self.buttonfunc(self.field.robotspace, 25000)  # pylint: disable=not-callable, unsubscriptable-object

    def periodic_update(self, gamepad):
        """Thread-function that updates the ControllerPos object."""
        print("Gamepad reader started!")
        while not EVENT.is_set():
            events = gamepad.read()
            for event in events:
                self.update(event)
            # print(controller_pos.position, end="\r")

    def converted(self):
        """Normalize input from different controllers."""
        sony_name = "Sony Interactive Entertainment Wireless Controller"
        xbox_name = "Microsoft X-Box 360 pad"
        pos_x = self.x
        pos_y = self.y
        if self.name == xbox_name:
            factor = 300/32767
            pos_x *= factor
            pos_y *= factor * -1
        elif self.name == sony_name:
            pos_x = (pos_x - 127) * (300/128)
            pos_y = (pos_y - 127) * (300/128) * -1
        factor2 = 0.2
        pos_x, pos_y = pos_x * factor2, pos_y * factor2
        return int(pos_x), int(pos_y)


class GUI(tkinter.Tk):
    """UI class."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.canvas = tkinter.Canvas(self, width=600, height=600)
        self.canvas.pack()
        # self.robot = robot

    def periodic_canvas_update(self, field, robot):
        """Write current position on canvas."""
        shape = None
        target = None

        while not EVENT.is_set():
            time.sleep(0.1)
            pos_x, pos_y = field.as_tuple()
            pos_x += 300
            # pos_y = 600 - (pos_y + 300)
            pos_y += 300
            self.canvas.delete(shape)
            self.canvas.delete(target)
            # shape = self.canvas.create_rectangle(pos_x-1, pos_y-1, pos_x + 1, pos_y+1)
            # shape = self.canvas.create_rectangle(pos_x-1, pos_y-1, pos_x + 1, pos_y+1)
            robopos = robot.current_position
            shape = self.canvas.create_line(pos_x, pos_y,
                                            robopos[0] + 300, 300 - robopos[1])
            target = self.canvas.create_oval(pos_x - 5, pos_y - 5,
                                             pos_x + 5, pos_y + 5)


class Position():
    """A two-tuple of x and y cooardinate with easier access methods."""
    def __init__(self):
        self.x = 0  # pylint: disable = invalid-name
        self.y = 0  # pylint: disable = invalid-name

    def __getitem__(self, index):
        return

    def as_tuple(self):
        """Position as a tuple."""
        return self.x, self.y

    def update_set(self, xpos=None, ypos=None):
        """Change position."""
        if xpos is not None:
            self.x = xpos
        if ypos is not None:
            self.y = ypos

    @property
    def robotspace(self):
        """Convert position to robotspace position."""
        return (self.x * 1, self.y * -1)

    def updater(self, controller_pos):
        """Read value from the Controller and move point on the playfield."""
        while not EVENT.is_set():
            time.sleep(0.05)
            pos = controller_pos.position
            self.x += round(pos[0] / 1000)
            self.y += round(pos[1] / 1000)
            self.x = clamp(-300, self.x, 300)
            self.y = clamp(-300, self.y, 300)
            # print(self.as_tuple())


def sendcommand(ser, command, waittime=0.1):
    """Send a G-Code to the robot."""
    if not command.endswith("\n"):
        command += "\n"
    ser.write(command.encode())
    time.sleep(waittime)
    print("Answer: {}".format(ser.readline().decode().strip()))
    while ser.in_waiting:
        print("Answer: {}".format(ser.readline().decode().strip()))


def clamp(min_, val, max_):
    """Clamp a value between a maximum and a minimum."""
    return max(min_, min(max_, val))


def printer(controller_pos):
    """Thread function that prints current position."""
    print("Printer started!")
    field_pos = [0, 0]
    while not EVENT.is_set():
        time.sleep(0.1)
        pos = controller_pos.position
        field_pos[0] += round(pos[0] / 1000)
        field_pos[1] += round(pos[1] / 1000)
        field_pos[0] = clamp(-300, field_pos[0], 300)
        field_pos[1] = clamp(-300, field_pos[1], 300)
        print(field_pos, end="                  \r")


def updater(field_pos, controller_pos):
    """Read value from the Controller and move point on the playfield."""
    while not EVENT.is_set():
        time.sleep(0.1)
        pos = controller_pos.position
        field_pos[0] += round(pos[0] / 1000)
        field_pos[1] += round(pos[1] / 1000)
        field_pos[0] = clamp(-300, field_pos[0], 300)
        field_pos[1] = clamp(-300, field_pos[1], 300)
        print(field_pos)



def main():
    """Main"""
    # pos_x, pos_y = 0, 0
    gamepad = inputs.devices.gamepads[0]
    print(gamepad.name)
    # ser = setup() TESTING
    controller_pos = ControllerPos(gamepad)
    robot = FakeRobot()
    robot = Robot()
    # field = [0, 0]
    field = Position()
    threads = []
    thread = threading.Thread(target=controller_pos.periodic_update, args=(gamepad,), daemon=True)
    thread.start()
    threads.append(thread)
    thread = threading.Thread(target=field.updater, args=(controller_pos,), daemon=True)
    thread.start()
    threads.append(thread)
    # thread = threading.Thread(target=printer, args=(controller_pos,), daemon=True)
    # thread = threading.Thread(target=mover, args=(ser, field), daemon=True)

    # thread = threading.Thread(target=robot.periodic_move, args=(field,), daemon=True)
    # thread.start()
    controller_pos.buttonfunc = robot.move
    controller_pos.field = field

    root = GUI()

    thread = threading.Thread(target=root.periodic_canvas_update, args=(field, robot), daemon=True)
    thread.start()
    threads.append(thread)

    root.mainloop()

    # for thread in threads:
    #     thread.join()

    # task2 = loop.create_task(printer(controller_pos))
    # task = loop.create_task(do_stuff_every_x_seconds(10, controller_pos, gamepad))
    # # loop.run_until_complete(task)
    # # loop.run_until_complete(task2)
    # loop.run_forever()

    EVENT.set()  # Let threads know they can terminate

def testmove(robot):
    """Move in a box pattern."""
    speed = 5000
    robot.move((-200, -200), speed)
    robot.move((200, -200), speed)
    robot.move((-200, 200), speed)
    robot.move((200, 200), speed)
    robot.move((0, 0), speed)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print()
