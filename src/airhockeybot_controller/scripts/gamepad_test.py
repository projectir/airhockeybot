"""Computer!"""

import time

import inputs
import serial

def sendcommand(ser, command, waittime=0.1):
    if not command.endswith("\n"):
        command += "\n"
    ser.write(command.encode())
    time.sleep(waittime)
    print("Answer: {}".format(ser.readline().decode().strip()))
    while ser.in_waiting:
        print("Answer: {}".format(ser.readline().decode().strip()))


def main():
    pos_x, pos_y = 0, 0
    gamepad = inputs.devices.gamepads[0]
    print(gamepad.name)
    ser = serial.Serial('/dev/ttyUSB0', 250_000)
    waittime = 0.1
    time.sleep(waittime)
    sendcommand(ser, "M906 X1500 Y1500 Z1500")
    sendcommand(ser, "M92 X-40 Y40 Z40")
    sendcommand(ser, "M203 X10000 Y10000 Z10000")
    sendcommand(ser, "G92 X0 Y0 Z0")
    # sendcommand(ser, "M122 S1\n")  # Debug
    sendcommand(ser, "M201 X100 Y100 Z100")
    sendcommand(ser, "M204 T500")  # Acceleration travel speed
    # sendcommand(ser, "G80")  # Cancel current motion
    print("Ready")
    speed = 5_000
    # testmove(ser)
    # return
    pos_x = pos_y = 0
    while True:

        # events = inputs.get_gamepad()
        events = gamepad.read()
        for event in events:
        #     if event.ev_type == "Sync":
        #         continue
        #     print(event.ev_type, event.code, event.state)
            if event.code in ("ABS_Y", "ABS_X"):
                if event.code == "ABS_X":
                    pos_x = event.state
                elif event.code == "ABS_Y":
                    pos_y = event.state
        events = gamepad.read()
        for event in events:
        #     if event.ev_type == "Sync":
        #         continue
        #     print(event.ev_type, event.code, event.state)
            if event.code in ("ABS_Y", "ABS_X"):
                if event.code == "ABS_X":
                    pos_x = event.state
                elif event.code == "ABS_Y":
                    pos_y = event.state
        # factor = 50/32767
        # factor = 50/255
        # factor = 1
        # print("Position: {0[0]:>6.0f}, {0[1]:>6.0f}".format(convert(pos_x, pos_y, gamepad.name)), end="\r")
        converted = convert(pos_x, pos_y, gamepad.name)
        move(ser, converted, speed)
        # x: minus ist links
        # y: minus ist oben


def testmove(ser):
    speed = 5000
    move(ser, (-200, -200), speed)
    move(ser, (200, -200), speed)
    move(ser, (-200, 200), speed)
    move(ser, (200, 200), speed)
    move(ser, (0, 0), speed)


def move(ser, pos, speed):
    message = "G0 X{pos[1]} Y{pos[1]} Z{pos[0]} F{speed}".format(pos=pos, speed=speed)
    print(message)
    sendcommand(ser, message, waittime=0)


def convert(pos_x, pos_y, controller="Microsoft X-Box 360 pad"):
    sony_name = "Sony Interactive Entertainment Wireless Controller"
    xbox_name = "Microsoft X-Box 360 pad"
    if controller == xbox_name:
        factor = 300/32767
        pos_x *= factor
        pos_y *= factor * -1
    elif controller == sony_name:
        pos_x = (pos_x - 127) * (300/128)
        pos_y = (pos_y - 127) * (300/128) * -1
    return int(pos_x), int(pos_y)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
