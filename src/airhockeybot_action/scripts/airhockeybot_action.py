#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""Action module, controls strategies and sends commands."""

import math
import traceback
import collections

import numpy
import rospy

from airhockeybot_msgs.msg import TrajectoryInformation
from airhockeybot_msgs.msg import BlockingPoint

from airhockeybot.config.project import ProjectConfiguration

Position = collections.namedtuple("Position", ["x", "y"])


class Strategy:
    """Abstract base strategy class."""
    config = ProjectConfiguration()
    height = config["globals"]["playing_field"]["height"]
    width = config["globals"]["playing_field"]["width"]
    goal_width = config["globals"]["playing_field"]["goal_width"]
    block_line_distance = config["globals"]["action"]["block_line_distance"]

    def __init__(self):
        rospy.init_node("airhockeybot_action")
        rospy.Subscriber("airhockeybot_trajectory_information",
                         TrajectoryInformation, self.callback)
        self.publisher = rospy.Publisher('/airhockeybot/blocking_point',
                                         BlockingPoint, queue_size=10)
        rospy.loginfo("Finished Initialization in ACTION!")
        rospy.spin()

    def callback(self, data):
        """Function that will be called by the Subscriber."""
        raise NotImplementedError


class Blocker(Strategy):
    """Simple strategy to place the puck between puck and goal."""

    def callback(self, data):
        try:
            _index, block_point = self.find_block_point_goal(data)
            if block_point is not None:
                block_msg = BlockingPoint()
                block_msg.x = block_point.x
                block_msg.y = block_point.y
                self.publisher.publish(block_msg)
        except Exception:
            tb = traceback.format_exc()
            rospy.logerr(tb)

    def find_block_point(self, trajectory_data):
        """Get the point where the robot can block the puck."""
        trajectories = trajectory_data.trajectories
        block_line = ((self.block_line_distance, 0), (self.block_line_distance, 300))
        for index, trajectory in enumerate(trajectories):
            start = trajectory.x_init, trajectory.y_init
            hit = trajectory.intersection.x, trajectory.intersection.y
            intersection = intersection_of_two_lines((start, hit), block_line)
            if intersection is None:
                return None, None  # Parallel or no movement
            # Is the intersection point on the board?
            if 0 <= intersection.y <= self.height:
                return index, intersection  # Stop after first found instance
        return None, None

    def find_block_point_goal(self, trajectory_data):
        """Find blocking point, but only in front of the goal."""
        trajectories = trajectory_data.trajectories
        block_line = ((self.block_line_distance, 0), (self.block_line_distance, 300))
        center = self.height / 2
        additional_room = (self.goal_width / 2) + self.block_line_distance
        for index, trajectory in enumerate(trajectories):
            start = trajectory.x_init, trajectory.y_init
            hit = trajectory.intersection.x, trajectory.intersection.y
            intersection = intersection_of_two_lines((start, hit), block_line)
            if intersection is None:
                return None, None  # Parallel or no movement
            # Is the intersection point on the board?
            if (
                    max(center - additional_room, 0) <=
                    intersection.y <=
                    min(center + additional_room, self.height)):
                return None, intersection  # Stop after first found instance
        return None, None


class Reflector(Blocker):
    """Assume blocking position, but try to counter-attack when puck is near."""
    def __init__(self):
        self.attacking = False
        self.previous = None
        super(Blocker, self).__init__()

    def callback(self, data):
        try:
            target = self.reflect(data)
            if target is not None:
                block_msg = BlockingPoint()
                block_msg.x = target.x
                block_msg.y = target.y
                self.publisher.publish(block_msg)
        except Exception:  # TODO reduce exception scope
            tb = traceback.format_exc()
            rospy.logerr(tb)

    def reflect(self, trajectory_data):
        """Reflect incoming puck."""
        index, block_point = self.find_block_point(trajectory_data)
        if block_point is None:
            return None
        # Is The puck headed to the enemy and out of reach? Block in center.
        if (trajectory_data.trajectories[0].x_vel > 0 and
                trajectory_data.trajectories[0].x_init > self.width * 0.5):
            home_position = Position(self.block_line_distance, self.height / 2)
            self.previous = home_position
            return Position(self.block_line_distance, self.height / 2)
        trajectory = trajectory_data.trajectories[0]
        start = Position(trajectory.x_init, trajectory.y_init)
        if (
                index == 0 and
                self.previous is not None and not self.attacking and
                # TODO Usable when we can know where the robot is
                #   distance_point_point(self.previous, CURRENTPOSITION) < 10 and
                distance_point_point(self.previous, start) < 500):  # MAGIC VALUE
            # self.attacking = True  # Uncomment on previous TODO
            return start
        # Uncomment on previous TODO
        # elif self.attacking and self.previous is not None:
        #     if distance_point_point(self.previous, CURRENTPOSITION) < 10:
        #         self.attacking = False
        #         return block_point  # Finished attack movement
        #     else:
        #         return self.previous  # Not yet reached
        self.previous = block_point
        return block_point

def intersection_of_two_lines(line1, line2):
    """Find the intersection point of two lines.

    Algorithm is described in
    https://en.wikipedia.org/wiki/Line–line_intersection#Given_two_points_on_each_line
    """
    # pylint: disable=invalid-name, bad-continuation
    x1, y1 = line1[0]
    x2, y2 = line1[1]
    x3, y3 = line2[0]
    x4, y4 = line2[1]
    try:
        px = (((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4)) /
                    ((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4)))
        py = (((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4)) /
                    ((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4)))
    except ZeroDivisionError:
        # Lines are parallel or line has same start/end point
        return None
    return Position(px, py)


def distance_point_line(point, line):
    """Shortest distance between a point and a line.

    https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points
    """
    # pylint: disable=invalid-name
    x0, y0 = point
    (x1, y1), (x2, y2) = line
    distance = (
        (abs((y2-y1)*x0-(x2-x1)*y0+x2*y1-y2*x1)) /
        (math.sqrt((y2-y1)**2+(x2-x1)**2)))
    return distance


def distance_point_point(point1, point2):
    """Euclidean distance between two points."""
    if isinstance(point1, Position) and isinstance(point2, Position):
        distance = numpy.linalg.norm((point1.x - point2.x, point1.y - point2.y))
    else:
        distance = numpy.linalg.norm((point1[0] - point2[0], point1[1] - point2[1]))
    return distance


if __name__ == "__main__":
    try:
        # Blocker()
        Reflector()
    except rospy.ROSInterruptException:
        rospy.logerr("ROSInterruptException")
