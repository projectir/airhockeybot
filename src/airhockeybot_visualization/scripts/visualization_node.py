#!/usr/bin/env python
"""Draw information onto the camera image and publish that."""

import collections

import numpy as np
import cv2
import rospy
from cv_bridge import CvBridge

from std_msgs.msg          import Int8
from sensor_msgs.msg       import Image
from airhockeybot_msgs.msg import TrajectoryInformation
from airhockeybot_msgs.msg import TransformationMatrix
from airhockeybot_msgs.msg import BlockingPoint
from airhockeybot_msgs.msg import ImageInformation

GREEN = (0, 255, 0)[::-1]
BLUE = (0, 255, 255)[::-1]
YELLOW = (255, 255, 0)[::-1]
ORANGE = (255, 128, 0)[::-1]

Position = collections.namedtuple("Position", ["x", "y"])


class StreamVisualizer:
    """Subscribe on image, draw points and lines and republish."""

    def __init__(self):
        """Build subscribers."""
        self.bridge = CvBridge()
        self.trajectories_msg = None
        self.transformation_to_image = None
        self.blocking_point = None
        self.image_information = None
        self.scores = {"Player": 0, "Bot": 0}

        rospy.init_node('visualize_on_stream', anonymous=True)
        rospy.Subscriber('/airhockeybot/bot_goals', Int8,
                         self.save_score_bot)
        rospy.Subscriber('/airhockeybot/player_goals', Int8,
                         self.save_score_player)
        rospy.Subscriber("airhockeybot_trajectory_information", TrajectoryInformation,
                         self.save_trajectories)
        rospy.Subscriber("/airhockeybot/blocking_point", BlockingPoint,
                         self.save_blocking_point)
        rospy.Subscriber("airhockeybot_marker_information", TransformationMatrix,
                         self.save_transformation_matrix)
        rospy.Subscriber("/usb_cam/image_raw", Image,
                         self.process_and_republish_image)
        rospy.Subscriber("/camera/color/image_raw", Image,
                         self.process_and_republish_simulation)
        rospy.Subscriber("airhockeybot_image_information", ImageInformation,
                         self.save_image_information)
        self.pub = rospy.Publisher('/visualization', Image, queue_size=10)
        rospy.loginfo("Initialized visualization.")
        rospy.spin()

    def save_blocking_point(self, data):
        self.blocking_point = data

    def save_trajectories(self, data):
        self.trajectories_msg = data

    def save_score_player(self, data):
        self.scores["Player"] = data.data

    def save_score_bot(self, data):
        self.scores["Bot"] = data.data

    def save_image_information(self, data):
        self.image_information = data

    def save_transformation_matrix(self, data):
        self.transformation_to_image = np.array(data.to_image).reshape((2, 3)).astype(np.float32)

    def process_and_republish_image(self, data):
        """Subscriber to a raw image, draws paths on image and publishes result."""
        frame = self.bridge.imgmsg_to_cv2(data, 'bgr8')

        frame = self._draw_trajectories(frame)
        frame = self._draw_blocking_point(frame)
        frame = self._draw_score(frame)

        img_msg = self.bridge.cv2_to_imgmsg(frame, "bgr8")
        img_msg.header.stamp = rospy.Time.now()
        img_msg.header.frame_id = "/visualization"
        self.pub.publish(img_msg)


    def process_and_republish_simulation(self, data):
        """Subscriber to a raw image, draws paths on image and publishes result."""
        frame = self.bridge.imgmsg_to_cv2(data, 'bgr8')
        if self.image_information is not None:
            (xpos, ypos) = np.round([self.image_information.puck_position.x,
                                     self.image_information.puck_position.y]
                                    ).astype("int")
            cv2.circle(frame, (xpos, ypos), 20, GREEN, 4)
            cv2.rectangle(frame, (xpos - 5, ypos - 5), (xpos + 5, ypos + 5), ORANGE, -1)

        frame = self._draw_trajectories(frame)
        frame = self._draw_blocking_point(frame)
        frame = self._draw_score(frame)

        img_msg = self.bridge.cv2_to_imgmsg(frame, "bgr8")
        img_msg.header.stamp = rospy.Time.now()
        img_msg.header.frame_id = "/visualization"
        self.pub.publish(img_msg)

    def _draw_trajectories(self, frame):
        """Draw the Trajectories onto a frame if any."""
        if (self.trajectories_msg is not None) and (self.transformation_to_image is not None):
            trajectories = self.trajectories_msg.trajectories
            for index, trajectory in enumerate(trajectories):
                start_img_coordinate = np.matmul(self.transformation_to_image,
                                                 np.array([trajectory.x_init, trajectory.y_init, 1]))
                x_init = start_img_coordinate[0]
                y_init = start_img_coordinate[1]

                intersection = trajectory.intersection
                intersection_img_coordinate = np.matmul(self.transformation_to_image,
                                                        np.array([intersection.x, intersection.y, 1]))
                x_intersection = intersection_img_coordinate[0]
                y_intersection = intersection_img_coordinate[1]
                # rospy.loginfo("%s %s %s %s", x_init, y_init, x_intersection, y_intersection)

                overlay = frame.copy()
                overlay = cv2.line(
                    overlay,
                    (int(x_init), int(y_init)),
                    (int(x_intersection), int(y_intersection)),
                    YELLOW, 5)
                alpha = 1 - (index / len(trajectories))
                frame = cv2.addWeighted(overlay, alpha, frame, 1 - alpha, 0)
        return frame

    def _draw_blocking_point(self, frame):
        """Draw the blocking point onto a frame if there is one."""
        if self.blocking_point is not None:
            img_coords = np.matmul(self.transformation_to_image, np.array(
                [self.blocking_point.x, self.blocking_point.y, 1]))
            frame = cv2.circle(frame,
                               (int(img_coords[0]), int(img_coords[1])), 20, YELLOW, 5)
        return frame

    def _draw_score(self, frame):
        """Draw the blocking point onto a frame if there is one."""
        if True:
            text = "%s : %s" % (self.scores["Bot"], self.scores["Player"])
            frame = cv2.putText(
                frame, text,
                org=(30, 30),
                fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                fontScale=1,
                color=GREEN,
                thickness=3)
        return frame


def dummy(*args, **kwargs):
    rospy.loginfo("Triggered dummy!!")


def dim_color(color, dim_factor):
    """Darken a color tuple."""
    new_color = tuple([value * dim_factor for value in color])
    return new_color

if __name__ == '__main__':
    try:
        visualizer = StreamVisualizer()
    except rospy.ROSInterruptException:
        pass
