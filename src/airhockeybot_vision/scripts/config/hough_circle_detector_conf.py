#!/usr/bin/env python3
"""Config for the Hough circle detector."""


class HoughCircleDetectorConfig(dict):
    """Wrapper class for the configuration of the Hough circle puck detection algorithm.

    This configuration defines hyperparameter choices
    for the Hough transform-based puck detection.
    For the hyperparameter configuration, see also
    https://docs.opencv.org/2.4/modules/imgproc/doc/feature_detection.html?highlight=houghcircles#houghcircles
    """

    def __init__(self, global_config):
        """Initialize this configuration defining hyperparameters.

        For the Hough circle detector algorithm.
        Hyperparameter choices depending on the current global configuration.
        """
        dict.__init__(self)
        img_width = global_config["camera"]["width"]
        img_height = global_config["camera"]["height"]
        if (img_width, img_height) == (1920, 1080):
            self["dp"] = 3
            self["minDist"] = 1920 # makes the algorithm only recognize one single puck
            self["minRadius"] = 29
            self["maxRadius"] = 33
        elif (img_width, img_height) == (1280, 720):
            self["dp"] = 3
            self["minDist"] = 1280 # makes the algorithm only recognize one single puck
            self["minRadius"] = 18
            self["maxRadius"] = 22
        elif (img_width, img_height) == (848, 480):
            self["dp"] = 3
            self["minDist"] = 848  # makes the algorithm only recognize one single puck
            self["minRadius"] = 12
            self["maxRadius"] = 16
        else:
            raise ValueError("Invalid image resolution, change to 1080p, 720p or 480p.")
