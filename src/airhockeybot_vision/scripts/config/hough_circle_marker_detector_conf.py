#!/usr/bin/env python3
"""Config for the Hough circle marker detector."""


class HoughCircleMarkerDetectorConfig(dict):
    """
    Wrapper class for configuration of the Hough circle marker detector.

    This configuration defines hyperparameter choices for the
    Hough transform-based marker detection.
    For the hyperparameter configuration, see also
    https://docs.opencv.org/2.4/modules/imgproc/doc/feature_detection.html?highlight=houghcircles#houghcircles
    """

    def __init__(self, global_config):
        """Initialize this configuration defining hyperparameters.

        For the Hough circle marker detectin algorithm. Hyperparameter choices
        depending on the current global configuration.
        """
        dict.__init__(self)
        img_width = global_config["camera"]["width"]
        img_height = global_config["camera"]["height"]
        if (img_width, img_height) == (1920, 1080):
            self["dp"] = 2
            self["minDist"] = 100
            self["minRadius"] = 89
            self["maxRadius"] = 92
        elif (img_width, img_height) == (1280, 720):
            self["dp"] = 2
            self["minDist"] = 100
            self["minRadius"] = 57
            self["maxRadius"] = 61
        elif (img_width, img_height) == (848, 480):
            self["dp"] = 2
            self["minDist"] = 50
            self["minRadius"] = 37
            self["maxRadius"] = 40
        else:
            raise ValueError("Invalid image resolution, change to 1080p, 720p or 480p.")
