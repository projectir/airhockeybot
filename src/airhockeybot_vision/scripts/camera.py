#!/usr/bin/env python3"""Reader for the camera."""

import subprocess
import numpy as np
import cv2

from detector.hough_circles_puck_detector import HoughCirclesPuckDetector
from airhockeybot.config.project import ProjectConfiguration

GREEN = (0, 255, 0)
ORANGE = (0, 128, 255)


def video_capture_factory(config):
    """Create an OpenCV VideoCapture object based on the current user configuration."""
    camera_index = config["globals"]["camera"]["input_device_id"]
    width, height = config["globals"]["camera"]["width"], config["globals"]["camera"]["height"]
    camera_props = config["globals"]["camera"]["props"]
    fps = config["globals"]["camera"]["frame_rate"]
    cap = cv2.VideoCapture(camera_index)
    cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    cap.set(cv2.CAP_PROP_FPS, float(fps))
    # set camera props in v4l2
    for key in camera_props:
        subprocess.call(['v4l2-ctl -d /dev/video{} -c {}={}'.format(str(camera_index), key, str(camera_props[key]))],
                    shell=True)
    rospy.loginfo("Initialized video capture on camera index '%s' \n"
                 "Resolution: %sx%s \n Video codec: MJPG \n FPS: %s \n Camera settings: %s",
                 camera_index, width, height, fps, camera_props)
    return cap


def main():
    """Recognize puck on camera."""
    config = ProjectConfiguration()
    cap = video_capture_factory(config)
    hough_circle_detector = HoughCirclesPuckDetector(config)
    # color_filtering_detector = ColorFilterPuckDetector(config)
    while True:
        # Capture frame-by-frame
        _ret, frame = cap.read()
        circles = hough_circle_detector.detect(frame)
        if circles is not None:
            # convert the (x, y) coordinates and radius of the circles to integers
            circles = np.round(circles[0, :]).astype("int")
            # loop over the (x, y) coordinates and radius of the circles
            for (xpos, ypos, radius) in circles:
                cv2.circle(frame, (xpos, ypos), radius, GREEN, 4)
                cv2.rectangle(frame,
                              (xpos - 5, ypos - 5),
                              (xpos + 5, ypos + 5), ORANGE, -1)
        # show the output image
        cv2.imshow("output", frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
