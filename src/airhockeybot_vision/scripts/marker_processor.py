#!/usr/bin/env python3
"""Processing markers to get plane-playfield conversion matrix."""

import collections
from timeit import default_timer as timer

import rospy
import cv2
from cv_bridge import CvBridge
import numpy as np

from sensor_msgs.msg import Image
from detector.hough_circles_marker_detector import HoughCirclesMarkerDetector
from detector.hough_circles_marker_detector_CUDA import HoughCirclesMarkerDetectorCUDA
from airhockeybot_msgs.msg import TransformationMatrix
from airhockeybot.config.project import ProjectConfiguration

CONFIG = ProjectConfiguration()

IMG_WIDTH = CONFIG["globals"]["camera"]["width"]
IMG_HEIGHT = CONFIG["globals"]["camera"]["height"]


class MarkerProcessor:
    """Process Markers, publish Transformation Matrix based on positions."""
    def __init__(self):
        """Initialize the node."""
        rospy.init_node("marker_detector", anonymous=True)
        use_sim = rospy.get_param("/airhockeybot_controller/use_simulation")
        if use_sim:
            rospy.Subscriber("/camera/color/image_raw", Image, self.on_image_receive)
        else:
            rospy.Subscriber("/usb_cam/image_raw", Image, self.on_image_receive)
        self.timer_buffer = collections.deque(maxlen=1000)
        self.marker_positions_buffer = collections.deque(
            maxlen=CONFIG["globals"]["detections"]["marker_image_avg"])
        self.bridge = CvBridge()  # for ros image_msg_to_cv2
        self.use_cuda = CONFIG["globals"]["opencv"]["use_cuda"]
        if self.use_cuda:
            self.hough_marker_detector = HoughCirclesMarkerDetectorCUDA(CONFIG)
        else:
            self.hough_marker_detector = HoughCirclesMarkerDetector(CONFIG)

        self.transformation_pub = rospy.Publisher(
            "airhockeybot_marker_information",
            TransformationMatrix, queue_size=10)

        # Initialize matrix containing positions
        pos = CONFIG["globals"]["playing_field"]["marker"]["positions"]
        marker_sw = np.array([pos["left_bottom"]["center_x"], pos["left_bottom"]["center_y"]])  # left-bottom
        marker_nw = np.array([pos["left_top"]["center_x"], pos["left_top"]["center_y"]])  # left-top
        marker_se = np.array([pos["right_bottom"]["center_x"], pos["right_bottom"]["center_y"]])  # right-bottom
        # marker_ne = np.array([pos["right_top"]["center_x"], pos["right_top"]["center_y"]])  # right-top
        self.marker_m = np.array([marker_sw, marker_nw, marker_se])

        rospy.loginfo("Finished Initialization")
        rospy.spin()

    @staticmethod
    def generate_transformation_matrix(to_playing_field, to_image):
        matrix = TransformationMatrix()
        matrix.to_world = to_playing_field.reshape(-1)
        matrix.to_image = to_image.reshape(-1)
        return matrix

    def on_image_receive(self, data):
        try:
            start = timer()

            frame = self.bridge.imgmsg_to_cv2(data, 'bgr8')
            if self.use_cuda:
                cuda_mat = cv2.cuda_GpuMat(frame)
                circles = self.hough_marker_detector.detect(cuda_mat).download()
            else:
                circles = self.hough_marker_detector.detect(frame)

            assert circles is not None, "No markers have been detected"
            circles = circles[0, :]  # only (x, y, radius) subset
            assert len(circles) == 4, "Not exactly 4 markers have been detected"
            positions = np.zeros((3,2))

            for (xpos, ypos, _radius) in np.round(circles).astype("int"):
                if xpos < IMG_WIDTH / 2 and ypos >= IMG_HEIGHT / 2:
                    positions[0,:] = [xpos, ypos] # bottom left
                if xpos < IMG_WIDTH / 2 and ypos < IMG_HEIGHT / 2:
                    positions[1,:] = [xpos, ypos] # top left
                if xpos >= IMG_WIDTH / 2 and ypos >= IMG_HEIGHT / 2:
                    positions[2,:] = [xpos, ypos] # bottom right
                # # only use 3 points
                # if xpos >= IMG_WIDTH / 2 and ypos < IMG_HEIGHT / 2:
                #     positions[3,:] = [xpos, ypos] #top right
            self.marker_positions_buffer.append(positions)

            # If buffer is full, publish transformation
            if len(self.marker_positions_buffer) == self.marker_positions_buffer.maxlen: # avg over n frames
                avg = sum(self.marker_positions_buffer) / len(self.marker_positions_buffer)
                to_playing_field = cv2.getAffineTransform(avg.astype(np.float32), self.marker_m.astype(np.float32))
                to_image = cv2.getAffineTransform(self.marker_m.astype(np.float32), avg.astype(np.float32))
                matrix_msg = self.generate_transformation_matrix(to_playing_field, to_image)
                self.marker_positions_buffer.clear() # empty buffer

                self.transformation_pub.publish(matrix_msg)

            # # debug purpose
            # warp_dst = cv2.warpAffine(frame, to_playing_field, (frame.shape[1], frame.shape[0]))
            # cv2.imshow('Warp', warp_dst)
            # cv2.waitKey()

            end = timer()
            self.timer_buffer.append(end - start)
            if len(self.timer_buffer) == 1000:
                avg = sum(self.timer_buffer) / len(self.timer_buffer)
                rospy.loginfo("[TRANFORMATION ESTIMATION]    Processing time in s, avg over 1000 runs: %s", avg)
                self.timer_buffer.clear()

        except AssertionError as e:
            # Would show info like "Not exactly 4 markers have been detected"
            rospy.logdebug(e)

if __name__ == "__main__":
    try:
        MarkerProcessor()
    except rospy.ROSInterruptException:
        pass
