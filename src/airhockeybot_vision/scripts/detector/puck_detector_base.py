#!/usr/bin/env python3
"""Puck detector module."""

class PuckDetectorBase:
    """Puck detector base class.

    Inherit your custom puck detector from this base class.
    Puck detectors can be implemented by various algorithms ranging from
    classical Computer Vision to state-of-the-art Deep Learning models.
    This base class defines a common interface to be used throughout
    the project to access various puck detection algorithms.

    Args:
        config: A ProjectConfiguration or a derived class instance
        giving access to environmental and algorithmic configurations.
        Examples are puck and tabletop constants like colors,
        camera intrinsics like frame rate and algorithmic hyperparameters.
    """
    def __init__(self, config, **_kwargs):
        self.project_config = config

    def detect(self, img):
        """Detect the air hockey puck in the given image.

        Return the image coordinates of the puck location.
        """
        raise NotImplementedError
