#!/usr/bin/env python3
"""Marker detection class."""

class MarkerDetectorBase:
    """This is the marker detector base class.

    Inherit your custom marker detector from this base class.
    This base class defines a common interface to be used throughout
    the project to access various marker detection algorithms.

    Args:
        config: A ProjectConfiguration or a derived class instance giving
        access to environmental and algorithmic configurations.
        Examples are puck and tabletop constants like colors, camera
        intrinsics like frame rate and algorithmic hyperparameters.
    """

    def __init__(self, config, **_kwargs):
        self.project_config = config

    def detect(self, img):
        """Detect the marker in the given image.

        Return the image coordinates of the marker locations.
        """
        raise NotImplementedError
