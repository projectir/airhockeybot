#!/usr/bin/env python3
"""Marker detector using hough."""

import cv2
from config.hough_circle_marker_detector_conf import HoughCircleMarkerDetectorConfig
from detector.marker_detector_base import MarkerDetectorBase


class HoughCirclesMarkerDetector(MarkerDetectorBase):
    """ Puck detector based on Hough transform.

        Args:
            config: A ProjectConfiguration or a derived class instance giving
            access to environmental and algorithmic configurations.
            Examples are puck and tabletop constants like colors, camera
            intrinsics like frame rate and algorithmic hyperparameters.
    """

    def __init__(self, config):
        MarkerDetectorBase.__init__(self, config)
        hough_config = HoughCircleMarkerDetectorConfig(config["globals"])
        self.project_config["hough_circle_marker"] = hough_config

    def detect(self, img):
        """ Detect the playing field marker in the given image.

            Args:
                img:    An OpenCV BGR-encoded image.
        """
        grayscale_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        config = self.project_config["hough_circle_marker"]
        circles = cv2.HoughCircles(
            grayscale_img,
            cv2.HOUGH_GRADIENT,
            dp=config["dp"],
            minDist=config["minDist"],
            minRadius=config["minRadius"],
            maxRadius=config["maxRadius"]
        )
        return circles
