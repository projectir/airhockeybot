#!/usr/bin/env python3
"""Hough Circle detector."""

import cv2

from config.hough_circle_detector_conf import HoughCircleDetectorConfig
from detector.puck_detector_base import PuckDetectorBase


class HoughCirclesPuckDetectorCUDA(PuckDetectorBase):
    """Puck detector based on Hough transform.

    Args:
        config: A ProjectConfiguration or a derived class instance giving
        access to environmental and algorithmic configurations.
        Examples are puck and tabletop constants like colors, camera
        intrinsics like frame rate and algorithmic hyperparameters.
    """

    detector = None

    def __init__(self, config):
        PuckDetectorBase.__init__(self, config)
        hough_config = HoughCircleDetectorConfig(config["globals"])
        self.project_config["hough_circle"] = hough_config
        config = self.project_config["hough_circle"]
        self.detector = cv2.cuda.createHoughCirclesDetector(config["dp"], config["minDist"], 1, 1,
                                                            config["minRadius"], config["maxRadius"])

    def detect(self, img):
        """Detect the air hockey puck in the given image.

        Args:
            img:    An OpenCV BGR-encoded image.
        """
        grayscale_img = cv2.cuda.cvtColor(img, cv2.COLOR_BGR2GRAY)
        circles = self.detector.detect(grayscale_img)
        return circles
