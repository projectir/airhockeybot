#!/usr/bin/env python3
"""Hough Circle detector."""

import cv2

from config.hough_circle_detector_conf import HoughCircleDetectorConfig
from detector.puck_detector_base import PuckDetectorBase


class HoughCirclesPuckDetector(PuckDetectorBase):
    """Puck detector based on Hough transform.

    Args:
        config: A ProjectConfiguration or a derived class instance giving
        access to environmental and algorithmic configurations.
        Examples are puck and tabletop constants like colors, camera
        intrinsics like frame rate and algorithmic hyperparameters.
    """
    def __init__(self, config):
        PuckDetectorBase.__init__(self, config)
        hough_config = HoughCircleDetectorConfig(config["globals"])
        self.project_config["hough_circle"] = hough_config

    def detect(self, img):
        """Detect the air hockey puck in the given image.

        Args:
            img:    An OpenCV BGR-encoded image.
        """
        grayscale_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        config = self.project_config["hough_circle"]
        circles = cv2.HoughCircles(
            grayscale_img,
            cv2.HOUGH_GRADIENT,
            dp=config["dp"],
            minDist=config["minDist"],
            minRadius=config["minRadius"],
            maxRadius=config["maxRadius"]
        )
        return circles
