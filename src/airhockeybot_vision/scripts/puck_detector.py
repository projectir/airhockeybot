#!/usr/bin/env python3
"""Puck detector node which works on a stream."""

import collections
import subprocess
from timeit import default_timer as timer

import rospy
import numpy
import cv2
from cv_bridge import CvBridge

from sensor_msgs.msg import Image
from sensor_msgs.msg import CameraInfo
from camera_info_manager.camera_info_manager import CameraInfoManager
from detector.hough_circles_puck_detector import HoughCirclesPuckDetector
from detector.hough_circles_puck_detector_CUDA import HoughCirclesPuckDetectorCUDA
from airhockeybot_msgs.msg import ImageInformation
from airhockeybot_msgs.msg import PuckPosition

from airhockeybot.config.project import ProjectConfiguration


GREEN = (0, 255, 0)
ORANGE = (0, 128, 255)

class PuckDetector:
    config = ProjectConfiguration()

    def __init__(self):
        try:
            if rospy.get_param('use_sim_time'):
                rospy.logerr("[Puck Detector] use_sim_time param had value true"
                             ", set to False automatic.")
                rospy.set_param('use_sim_time', False)
        except KeyError:
            rospy.logerr("No sim time param found.")
        # Set up node
        # rospy.init_node("usb_cam", anonymous=True)
        rospy.init_node("puck_detector", anonymous=True)
        self.start_time = rospy.get_time()
        self.timer_buffer = collections.deque(maxlen=100)
        self.bridge = CvBridge()  # for ros image_msg_to_cv2
        self.use_cuda = self.config["globals"]["opencv"]["use_cuda"]
        # Publish to
        self.image_publisher = rospy.Publisher(
            "/usb_cam/image_raw", Image, queue_size=10)
        self.detection_publisher = rospy.Publisher(
            "airhockeybot_image_information", ImageInformation, queue_size=10)
        self.info_publisher = rospy.Publisher(
            "/usb_cam/camera_info", CameraInfo, queue_size=10)
        self.info_manager = CameraInfoManager(cname="usb_cam", namespace="usb_cam")

        self.info_manager.loadCameraInfo()

        if self.use_cuda:
            self.hough_circle_detector = HoughCirclesPuckDetectorCUDA(self.config)
        else:
            self.hough_circle_detector = HoughCirclesPuckDetector(self.config)

        if rospy.get_param("/puck_detector/use_sim"):
            rospy.Subscriber("/camera/color/image_raw", Image, self.find_puck_position)
        else:
            self.capture_loop()
        rospy.loginfo("Finished Initialization")
        rospy.spin()

    def video_capture_factory(self):
        """Create an OpenCV VideoCapture object based on the current user configuration."""
        config = self.config
        camera_index = config["globals"]["camera"]["input_device_id"]
        width = config["globals"]["camera"]["width"]
        height = config["globals"]["camera"]["height"]
        camera_props = config["globals"]["camera"]["props"]
        fps = config["globals"]["camera"]["frame_rate"]

        cap = cv2.VideoCapture(camera_index)
        cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        cap.set(cv2.CAP_PROP_FPS, float(fps))

        if isinstance(camera_index, int): # is video input index
            # set camera props in v4l2
            for key, value in camera_props.items():
                command = ["v4l2-ctl",
                           "-d", "/dev/video{}".format(camera_index),
                           "-c", "{}={}".format(key, value)]
                rospy.loginfo(" ".join(command))
                subprocess.call(command)
        rospy.loginfo("Initialized video capture on camera index '%s' \n"
                      "Resolution: %sx%s \n Video codec: MJPG \n"
                      "FPS: %s \n Camera settings: %s",
                      camera_index, width, height, fps, camera_props)
        return cap

    @staticmethod
    def generate_image_information(x, y, start_time):
        """Create ImageInformation object."""
        image_information = ImageInformation()
        puck_position = PuckPosition()
        puck_position.x = x
        puck_position.y = y
        puck_position.t = rospy.get_time() - start_time
        image_information.puck_position = puck_position
        return image_information

    def publish_frame(self, frame):
        """Send a frame over the image publisher."""
        try:
            img_msg = self.bridge.cv2_to_imgmsg(frame, "bgr8")
            img_msg.header.stamp = rospy.Time.now()
            img_msg.header.frame_id = "usb_cam"
            self.image_publisher.publish(img_msg)
            return
        except Exception as e:
            rospy.logerr("Image publishing failed, %s", e)

    def publish_camera_info(self):
        """Send camera info to publisher."""
        try:
            cam_info_msg = self.info_manager.getCameraInfo()
            cam_info_msg.header = rospy.Time.now()
            self.info_publisher.publish(cam_info_msg)
        except Exception as e:
            rospy.logerr("Camera Info publishing failed, %s", e)

    def find_puck_position(self, data):
        try:
            start = timer()
            frame = self.bridge.imgmsg_to_cv2(data, 'bgr8')

            # Detect puck
            if self.use_cuda:
                cuda_mat = cv2.cuda_GpuMat(frame)
                circles = self.hough_circle_detector.detect(cuda_mat).download()
            else:
                circles = self.hough_circle_detector.detect(frame)

            if circles is None or circles[0, :].shape[0] != 1:
                return
            circles = circles[0, :] # only (x, y, radius)
            (xpos, ypos, _radius) = numpy.round(circles[0]).astype("int")

            # Publish the puck detection
            image_information = self.generate_image_information(xpos, ypos, self.start_time)
            self.detection_publisher.publish(image_information)

            end = timer()
            self.timer_buffer.append(end - start)
            if len(self.timer_buffer) == self.timer_buffer.maxlen:
                avg = sum(self.timer_buffer) / len(self.timer_buffer)
                rospy.loginfo("[PUCK DETECTION]    Processing time in s, avg over 100 runs: %s", avg)
                self.timer_buffer.clear()
        except AssertionError as e:
            rospy.logerr(e)

    def capture_loop(self):
        """Recognize puck on camera."""
        cap = self.video_capture_factory()
        if self.use_cuda:
            hough_circle_detector = HoughCirclesPuckDetectorCUDA(self.config)
        else:
            hough_circle_detector = HoughCirclesPuckDetector(self.config)
        while not rospy.is_shutdown():  # main loop
            start = timer()
            _ret, frame = cap.read()
            if frame is None:
                continue
            # detect the puck
            if self.use_cuda:
                cuda_matrix = cv2.cuda_GpuMat(frame)
                circles = hough_circle_detector.detect(cuda_matrix).download()
            else:
                circles = hough_circle_detector.detect(frame)

            if circles is None or circles[0, :].shape[0] != 1:
                # Publish just the image
                self.publish_frame(frame)
                self.publish_camera_info()
                continue
            circles = circles[0, :] # only (x, y, radius)
            (xpos, ypos, radius) = numpy.round(circles[0]).astype("int")

            # Publish the puck detection
            image_information = self.generate_image_information(xpos, ypos, self.start_time)
            self.detection_publisher.publish(image_information)

            # Publish the image with detection
            cv2.circle(frame, (xpos, ypos), radius, GREEN, 4)
            cv2.rectangle(frame, (xpos - 5, ypos - 5), (xpos + 5, ypos + 5), ORANGE, -1)

            self.publish_frame(frame)
            self.publish_camera_info()

            #rate.sleep()  # when it reaches more than FPS than specified; max-FPS lock
            end = timer()
            self.timer_buffer.append(end - start)
            if len(self.timer_buffer) == 100:
                avg = sum(self.timer_buffer) / len(self.timer_buffer)
                rospy.loginfo("[PUCK DETECTION]    Processing time in s, avg over 100 detections: %s", avg)
                self.timer_buffer.clear()

        # When everything done, release the capture
        cap.release()

if __name__ == "__main__":
    try:
        PuckDetector()
    except rospy.ROSInterruptException:
        pass
