#!/usr/bin/env python
# license removed for brevity
import rospy
from cv_bridge import CvBridge
import cv2
from sensor_msgs.msg import Image
import numpy as np
from airhockeybot_msgs.msg import TransformationMatrix


trajectory_data = []
transformation_to_world = None
transformation_to_image = None
pub = rospy.Publisher('/usb_cam/image_raw/visualize_transformation', Image, queue_size=10)

bridge = CvBridge()

def trajectory_callback(data):
    global transformation_to_world
    transformation_to_world = np.array(data.to_world).reshape((2, 3))
    transformation_to_world = transformation_to_world.astype(np.float32)
    global transformation_to_image
    transformation_to_image = np.array(data.to_world).reshape((2, 3))
    transformation_to_image = transformation_to_world.astype(np.float32)

def image_callback(data):
    frame = bridge.imgmsg_to_cv2(data, 'bgr8')
    if transformation_to_world is None:
        rospy.logerr("Visualize Transformation: TransformationMatrix has not been set yet.")
        return
    # leave hand-coded (1805.0, 890.0) as we look for a image of this size! ()
    frame = cv2.warpAffine(frame, transformation_to_world, (1805, 890))
    #cv2.imshow("output", frame)
    #if cv2.waitKey(1) & 0xFF == ord('q'):
        #break
    img_msg = bridge.cv2_to_imgmsg(frame, "bgr8")
    img_msg.header.stamp = rospy.Time.now()
    img_msg.header.frame_id = "/usb_cam/image_raw/visualize_transformation"
    pub.publish(img_msg)

def initialize():
    rospy.init_node('visualize_transformation', anonymous=True)
    rospy.Subscriber("airhockeybot_marker_information", TransformationMatrix, trajectory_callback)
    rospy.Subscriber("/usb_cam/image_raw", Image, image_callback)
    rospy.spin()

if __name__ == '__main__':
    try:
        initialize()
    except rospy.ROSInterruptException:
        pass
