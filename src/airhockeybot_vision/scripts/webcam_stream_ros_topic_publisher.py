#!/usr/bin/env python3
import rospy
import cv2
import subprocess
import camera_info_manager.camera_info_manager as camera_info_manager
import collections

from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from sensor_msgs.msg import CameraInfo
from airhockeybot.config.project import ProjectConfiguration
from timeit import default_timer as timer
from airhockeybot.config.project import ProjectConfiguration

bridge = CvBridge() # for ros image_msg_to_cv2
config = ProjectConfiguration()

def init_ros_node():
    # Set up node
    rospy.init_node("webcam_stream_publisher", anonymous=True)
    start_time = rospy.get_time()
    # Publish to
    img_pub = rospy.Publisher("/usb_cam/image_raw", Image, queue_size=10)
    info_pub = rospy.Publisher("/usb_cam/camera_info", CameraInfo, queue_size=10)

    info_manager = camera_info_manager.CameraInfoManager(cname="usb_cam",
                                                         namespace="usb_cam")
    info_manager.loadCameraInfo()
    return img_pub, info_pub, info_manager, start_time

def video_capture_factory(config):
    """Create an OpenCV VideoCapture object based on the current user configuration."""
    camera_index = config["globals"]["camera"]["input_device_id"]
    width, height = config["globals"]["camera"]["width"], config["globals"]["camera"]["height"]
    camera_props = config["globals"]["camera"]["props"]
    fps = config["globals"]["camera"]["frame_rate"]
    cap = cv2.VideoCapture(camera_index)
    cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    cap.set(cv2.CAP_PROP_FPS, float(fps))
    # set camera props in v4l2
    for key in camera_props:
        print('v4l2-ctl -d /dev/video{} -c {}={}'.format(str(camera_index), key, str(camera_props[key])))
        subprocess.call(['v4l2-ctl -d /dev/video{} -c {}={}'.format(str(camera_index), key, str(camera_props[key]))],
                    shell=True)
    rospy.loginfo("Initialized video capture on camera index '%s' \n"
                 "Resolution: %sx%s \n Video codec: MJPG \n FPS: %s \n Camera settings: %s",
                 camera_index, width, height, fps, camera_props)
    return cap

def publish_frame(img_publisher, frame):
    try:
        img_msg = bridge.cv2_to_imgmsg(frame, "bgr8")
        img_msg.header.stamp = rospy.Time.now()
        img_msg.header.frame_id = "usb_cam"
        img_publisher.publish(img_msg)
        return
    except Exception as e:
        rospy.logerr("Image publishing failed, %s", e)

def publish_camera_info(info_publisher, info_manager):
    try:
        cam_info_msg = info_manager.getCameraInfo()
        cam_info_msg.header = rospy.Time.now()
        info_publisher.publish(cam_info_msg)
        return
    except Exception as e:
        rospy.logerr("Camera Info publishing failed, %s", e)

def main():
    """Recognize puck on camera."""
    cap = video_capture_factory(config)
    image_pub, info_pub, info_manager, start_time = init_ros_node()
    rate = rospy.Rate(config["globals"]["camera"]["frame_rate"]) # max frame rate

    TIMER_BUFFER = collections.deque(maxlen=1000)
    while not rospy.is_shutdown():
        start = timer()
        _ret, frame = cap.read()
        if frame is None:
            continue
        # Publish just the image
        publish_frame(image_pub, frame)
        publish_camera_info(info_pub, info_manager)

        #rate.sleep()  # when it reaches more than FPS than specified; max-FPS lock
        end = timer()
        TIMER_BUFFER.append(end - start)
        if len(TIMER_BUFFER) == 1000:
            avg = sum(TIMER_BUFFER) / len(TIMER_BUFFER)
            rospy.loginfo("[WEBCAM STREAM TO ROS NODE]    Processing time in s, avg over 1000 frames: %s", avg)
            TIMER_BUFFER.clear()

    # When everything done, release the capture
    cap.release()

if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass
