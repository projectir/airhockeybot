# airhockeybot_vision package

## Overview

This package is dedicated to detect the puck and to publish it's position, relative to the Airhockeybot playing field, to some topic. Detected puck position, for each frame of the input webcam stream (webcam like Logitech StreamCam or simulated webcam) are published and consumed by AirhockeyBot's [trajectory module](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_trajectory) into a puck trajectory estimate.

![Drag Racing](./../../docs/img/airhockey_table_with_puck_detection.jpg)

**Keywords:** Object Detection, Fast Moving Object Tracking, Computer Vision, Affine Transformation Estimation, Circle Hough Transform 

### Credentials

**Author: Marcus Rottschäfer, Jan-Gerrit Habekost, Jan Synwoldt<br />
Affiliation: [Masterproject 20/21 @ Technical Aspects of Multimodal Systems (TAMS), Department of Informatics, University of Hamburg](https://tams.informatik.uni-hamburg.de/) <br/>
Maintainer: Marcus Rottschäfer, 9rottsch@informatik.uni-hamburg.de<br />
Jan-Gerrit Habekost, 9habekos@informatik.uni-hamburg.de**

The airhockeybot_vision package has been tested under [ROS] Melodic on respectively Ubuntu 18.04 and 20.04.
This is research code, expect that it changes often and any fitness for a particular purpose is disclaimed.


### Publications

If you are interested in an overview of related work, regarding tracking of fast moving objects and approaches to airhockey puck detection in particular, please see the following seminar paper:

* M. Rottschäfer: **Real-time Fast Moving Object Tracking in Air Hockey**. 2020. ([PDF](https://gitlab.com/projectir/airhockeybot/-/blob/master/docs/pdf/Rottschaefer_2020_Real_time_Fast_Moving_Object_Tracking_in_Air_Hockey.pdf))

```
@techreport{Rottschaefer2020,     
    title = {{Real-time Fast Moving Object Tracking in Air Hockey}},
    year = {2020},
    author = {Rottsch{\"{a}}fer, Marcus Philip},
    pages = {7},
    url = {gitlab.com/projectir/airhockeybot/-/blob/master/docs/pdf/Rottschaefer_2020_Real_time_Fast_Moving_Object_Tracking_in_Air_Hockey.pdf},
    institution = {Masterproject Intelligent Robotics 20/21, Technical Aspects of Multimodal Systems (TAMS), Department of Informatics, University of Hamburg},
    address = {Hamburg}
}
```

### Dependencies

#### Third-party packages
- [Robot Operating System (ROS)](http://wiki.ros.org) (middleware for robotics),
- [OpenCV](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html) (computer vision library)
- [Numpy](https://scikit-learn.org/stable/) (linear algebra library)
- [camera info manager](http://wiki.ros.org/camera_info_manager) (camera calibration information)
- [cv_bridge](http://wiki.ros.org/cv_bridge/Tutorials/ConvertingBetweenROSImagesAndOpenCVImagesPython) (converting ROS images into OpenCV images and vice versa) 


#### AirhockeyBot packages
- [airhockeybot_msgs](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_msgs) (global config package)
- [airhockeybot](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot) (Custom ROS communication datatypes)

## Usage

To start up the whole Airhockeybot system follow the [project's ReadMe](https://gitlab.com/projectir/airhockeybot/-/tree/master). 
The airhockeybot_vision package can also be executed independently by following the instructions below.

#### Launch files
Run the main node with

	roslaunch airhockeybot_vision vision.launch

#### Config files

This project maintains all static values of the airhockey table, as well as system constants in [globals.json](https://gitlab.com/projectir/airhockeybot/-/blob/master/globals.json). Note that the file has to be correctly adapted to your custom setup, in order to make the `airhockeybot_vision` package work. When you want to apply the `airhockeybot_vision` for a given video, you can, instead of providing a webcam input ID in [globals.json](https://gitlab.com/projectir/airhockeybot/-/blob/master/globals.json) -> camera -> input-device-id, provide a full link to the video file. For example: `"input_device_id": "/home/shuka/videos/record_01_720p.avi"`.

#### Development

The main classes in this package are `PuckDetector` ([see docs](https://projectir.gitlab.io/airhockeybot/classpuck__detector_1_1PuckDetector.htmll)) and the specific CircleHoughTransform  implementations `HoughCirclesPuckDetector` ([see docs](https://projectir.gitlab.io/airhockeybot/classdetector_1_1hough__circles__puck__detector_1_1HoughCirclesPuckDetector.html)) and `HoughCirclesMarkerDetector` ([see docs](https://projectir.gitlab.io/airhockeybot/classdetector_1_1hough__circles__marker__detector_1_1HoughCirclesMarkerDetector.html)). For the affine transformation estimation (mapping image coordinates [of a puck detection] into playing field coordinates), the `MarkerProcessor` ([see docs](https://projectir.gitlab.io/airhockeybot/classmarker__processor_1_1MarkerProcessor.html)) is important.

Note that there are also CUDA versions available for each of the detectors described above, provided you compiled your OpenCV with CUDA support.


## Nodes
Here we describe the most important nodes and what they do.

### 1) /puck_detector

Detects the puck in the image and reports it's position. 

When using a webcam/video file: Uses OpenCV to read the input stream's frames and published them to `/usb_cam/image_raw` after detecting the puck (used for faster detections).

When using frames from the simulation: Listens to topic `/camera/color/image_raw` for incoming frames and detects the puck. Forwards the image with the detection to `/usb_cam/image_raw`.

#### 1.1) Subscribed Topics

* **`/camera/color/image_raw`** 

	Raw image provided by the simulation module.
	
#### 1.2) Published Topics

* **`/airhockeybot_image_information `** ([airhockeybot_msgs/ImageInformation](https://gitlab.com/projectir/airhockeybot/-/blob/master/src/airhockeybot_msgs/msg/ImageInformation.msg))

	The (image coordinate) puck detection in the current frame. 

* **`/usb_cam/image_raw`** 

	The current frame the detection has been applied on (either webcam/video, or simulation) with the detection highlighted optionally. 
	
	
* **`/usb_cam/camera_info`** 

	CameraInfoManager-related data, not important for the detection. See the corresponding ROS package for more details (section Dependencies).

#### Parameters
All parameters can be set in [globals.json](https://gitlab.com/projectir/airhockeybot/-/blob/master/globals.json). Object `camera` encapsulates all detection-related parameters:

* **`input_device_id `** (int/str, default: 0)

    ID of the input device (when using a webcam for input). Can also be a full path to a video file. When you want to use the simulation image stream as input instead, you need to set the correspondig parameter `use_sim in [airhockeybot.launch](https://gitlab.com/projectir/airhockeybot/-/blob/master/src/airhockeybot/launch/airhockeybot.launch).

* **`frame_rate `** (int, default: 60)

    Frame rate used for the detection. Assuming, the input stream provides the specified FPS; could also be less.
    
* **`width `** (int, default: 1280)

    Width of the input stream frames in pixels. We also (next to 720p, default) support 1080p and 480p input resolution.
   
* **`height `** (int, default: 720)

    Height of the input stream frames in pixels. We also (next to 720p, default) support 1080p and 480p input resolution.
    
* **`props `** (object, default: see [globals.json](https://gitlab.com/projectir/airhockeybot/-/blob/master/globals.json))

    Various camera-related parameters used to adjust the input webcam stream. Use this e.g. to set exposure time (better to have a short one for sharper images when the puck moves), brightness, contrast, etc. Best to leave them default if you place the robot in a bright environment. Only applies to webcam stream input. The parameters are set for the current webcam device at runtime, using the v4l2-ctl software package.




### 2) /marker_processor

Listens to incoming images, detects the markers in the playing field and calculates the affine transformation matrix used to derive a [image coordinate -> playing field coordinate] transformation, so that the puck's position can be obtained w.r.t the playing field, not the webcam frame. Uses hand-measured distances in the playing field (see constants in [globals.json](https://gitlab.com/projectir/airhockeybot/-/blob/master/globals.json)) to derive the mapping. 

Publishes the transformation matrix at a variable frequency (set in [globals.json](https://gitlab.com/projectir/airhockeybot/-/blob/master/globals.json) -> detection -> marker-image-avg, over how many frames should be averaged before publishing a transformation matrix).


#### 2.1) Subscribed Topics

* **`/usb_cam/image_raw`** 

	Raw image provided by the webcam.

* **`/camera/color/image_raw`** 

	Raw image provided by the simulation module.


#### 2.2) Published Topics

* **`/airhockeybot_marker_information `** ([airhockeybot_msgs/TransformationMatrix](https://gitlab.com/projectir/airhockeybot/-/blob/master/src/airhockeybot_msgs/msg/TransformationMatrix.msg))

	Affine transformation matrix estimate.