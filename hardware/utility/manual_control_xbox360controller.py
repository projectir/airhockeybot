# https://pypi.org/project/xbox360controller/
# pip3 install -U xbox360controller
# might have to be run with sudo

import serial
from xbox360controller import Xbox360Controller
from time import sleep

# !!! TODO Check for real values !!!
# values range from 0 to limit as absoulute coordinates
x_limit = 250
y_limit = 250

# TODO adjust as preference
speed = 2

# TODO check for correct port where g codes should be sent
serial_port = '/dev/ttyUSB0'
serial_baudrate = 250000

# !!! dont change values below !!!

x_current = 0
y_current = 0

x_direction = 0
y_direction = 0

serial_connection = serial.Serial(serial_port, serial_baudrate)

def update_direction(axis):
    global x_direction
    global y_direction

    x_direction = axis.x
    y_direction = axis.y


def move_robot():
    global x_limit
    global y_limit

    global x_current
    global y_current

    global x_direction
    global y_direction

    global speed

    x_new = x_current + speed * x_direction
    x_current = min(x_limit, max(0, x_new))

    y_new = y_current + speed * y_direction
    y_current = min(y_limit, max(0, y_new))

    print('{0} {1}'.format(x_current, y_current))

    gcode = 'G1 X{0} Y{1}'.format(int(x_current), int(y_current))
#    send_gcode(gcode)

def send_gcode(line):
    global serial_connection

    serial_connection.write( line + '\n')
    output = serial_connection.readline()
    print('> ' + output.strip())


# Set positioning mode to absolute coordinates
send_gcode('G90')

# TODO maybe move to (0,0) or (limit/2, limit/2) at the beginning,
# else robot will run into edges

try:
    with Xbox360Controller(0, axis_threshold=0.2) as controller:

        # callback function, when axis changes, update direction
        controller.axis_l.when_moved = update_direction

        while True:
            sleep(0.05)
            move_robot()

#       signal.pause()
except KeyboardInterrupt:
    pass
