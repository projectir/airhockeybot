#source devel/setup.bash
REBUILD=$1
CUSTOM_CV=$2
STATISTICS=$3

rosclean purge -y

if [ "$REBUILD" = "on" ]
then
    if [ "$CUSTOM_CV" = "on" ]
    then
      catkin clean -y
      catkin build --cmake-args -DOPENCV_CUSTOM_BUILD=True
    else
      catkin clean -y
      catkin build
    fi
fi

source devel/setup.bash

if [ "$STATISTICS" = "on" ]
then
  rosrun rqt_graph rqt_graph &
fi

roslaunch airhockeybot airhockeybot.launch
