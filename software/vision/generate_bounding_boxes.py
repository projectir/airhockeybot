from os.path import dirname, realpath, isfile
import numpy as np
import cv2
import os
from tqdm import tqdm
from save_old_vision_system.config import ProjectConfiguration
from save_old_vision_system.detector.marker.hough_circles_marker_detector import HoughCirclesMarkerDetector
from save_old_vision_system.detector import HoughCirclesPuckDetector
import shutil

CONFIG_DIR = dirname(dirname(dirname(dirname(os.path.abspath(__file__)))))
base_dir = str(dirname(dirname(dirname(realpath(__file__))))) + '/data/airhockeybot_dataset/'
groundtruth_dir = base_dir + 'output/groundtruth/'
labeled_dir = base_dir + 'output/labeled/'
unlabeled_dir = base_dir + 'output/unlabeled/'
folder_dirs = []
image_dirs = []
config = ProjectConfiguration(os.path.join(CONFIG_DIR, "airhockeybot/globals.json"))


def detect_circle(image_path):
    marker_detector = HoughCirclesMarkerDetector(config)
    puck_detector = HoughCirclesPuckDetector(config)
    frame = cv2.imread(image_path, 1)
    puck_circle = puck_detector.detect(frame)
    circles = puck_circle
    if circles is not None:
        # convert the (x, y) coordinates and radius of the circles to integers
        circles = np.round(circles[0, :]).astype("int")
        # loop over the (x, y) coordinates and radius of the circles
        return circles
    return None


def load_files():
    folders = os.listdir(base_dir)
    folders.remove('output')
    for subdirectory in folders:
        folder_dirs.append(base_dir + subdirectory)
    i = 0
    for path in folder_dirs:
        folder_content = os.listdir(path)
        for image in folder_content:
            image_path = path + '/' + image
            if isfile(image_path):
                i += 1
                image_dirs.append(image_path)
    print("Loaded " + str(i) + " images")


def cp_file_and_rename(img_path, labeled, img_count):
    if labeled:
        shutil.copy(img_path, labeled_dir + img_count + ".jpg")
    else:
        shutil.copy(img_path, unlabeled_dir + img_count + ".jpg")


def create_ground_truth(circle, img_count):
    x_pixel = circle[0][0]
    y_pixel = circle[0][1]
    radius = circle[0][2]

    picture_width = config["globals"]["camera"]["width"]
    picture_height = config["globals"]["camera"]["height"]

    left = (x_pixel - radius) / picture_width
    top = (y_pixel - radius) / picture_height
    width = 2 * radius / picture_width
    height = 2 * radius / picture_height

    try:
        groundtruth = open(groundtruth_dir + img_count + ".txt", "x")
        groundtruth.write("puck " + str(left) + " " + str(top) + " " + str(width) + " " + str(height))
    except FileExistsError:
        groundtruth = open(groundtruth_dir + img_count + ".txt", "w")
        groundtruth.write("puck " + str(left) + " " + str(top) + " " + str(width) + " " + str(height))
    groundtruth.close()


def main():
    load_files()
    fail = 0
    success = 0
    img_count = 0
    for image in tqdm(image_dirs):
        img_count += 1
        format_img_count = "%06d" % img_count
        detection_successful = False

        puck_information = detect_circle(image)
        if puck_information is not None: detection_successful = True
        cp_file_and_rename(image, detection_successful, format_img_count)

        if detection_successful:
            success += 1
            create_ground_truth(puck_information, format_img_count)
        else:
            fail += 1

    print("Fails: " + str(fail))
    print("Success: " + str(success))
    print("Sum: " + str(fail + success))


if __name__ == '__main__':
    main()
