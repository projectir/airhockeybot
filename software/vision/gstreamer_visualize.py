import cv2
import sys

reciever = 'udpsrc port=5000 caps = "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)JPEG, payload=(int)96" ! rtpjpegdepay ! jpegdec ! decodebin ! videoconvert ! appsink'

def receive():
    cap_receive = cv2.VideoCapture(reciever, cv2.CAP_GSTREAMER)

    if not cap_receive.isOpened():
        print('VideoCapture not opened')
        sys.exit(0)

    while True:
        ret,frame = cap_receive.read()

        if not ret:
            print('empty frame')
            break

        cv2.imshow('receive', frame)
        if cv2.waitKey(1)&0xFF == ord('q'):
            break

    cap_receive.release()
    if cv2.waitKey(1) & 0xFF == ord('q'):
        return

if __name__ == '__main__':
    #print(cv2.getBuildInformation())
    receive()
    cv2.destroyAllWindows()
