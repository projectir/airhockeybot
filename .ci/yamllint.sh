#!/bin/sh

#set -e

echo "Doing yamllint..."
bool=false
noRedundancyPrinting=false
filePWD=""
# explicitly set IFS to contain only a line feed
IFS='
'
for line in $(yamllint -d ./.ci/yaml_rules.yaml -s .); do
  #catch file name
  if echo "$line" | grep -E -q ".*\.\/.*(\.yml|\.yaml)" ; then
    filePWD="$line"
    noRedundancyPrinting=false
  else
    if ! grep -q "$(echo "$line" | sed 's/^[0-9]*:[0-9]*[ ]*//')" ".ci/yaml_accepted"; then
      if ! $noRedundancyPrinting ; then
        noRedundancyPrinting=true
        echo "$filePWD"
      fi
      echo "$line"
      bool=true
    fi
  fi
done
if $bool; then
  exit 1
else
  echo "No yamllint errors found."
fi

exit 0
