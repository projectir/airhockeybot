#!/bin/sh

echo "Doing pylint..."
bool=false
# explicitly set IFS to contain only a line feed
IFS='
'
filelist="$(find . -type f ! -name "$(printf "*\n*")")"
for line in $filelist; do
  if echo "$line" | grep -E -q ".*\.\/.*\.py" ; then
    if ! grep -q "$line" ".ci/python_accepted"; then
      if ! pylint -d C0301 -d C0114 -d C0115 -d C0116 -d C0103 -d E0401 -d R0903 -d R0914 -d R0913 -d R0902 -d R0201 -d E0602 -d C0411 -d W0511 -d W0107 -d R1710 -d W0613 -d W0621 -d R0912 -d W0125 -d R1728 -d W0622 -d E0012 -d W0703 -d W0612 -d E1003 -d W0201 -d R0904 -d E0001 -d C0325 -d W0402 -d C0415 -d E0611 -d W0707 -d W0404 -d W1505 -d W0603 -d E1101 -d C0200 -d R1716 -d C0321 -d W1401 -d W0401 -d R1705 -d W0106 -d W1309 -d R1732 "$line"; then
        bool=true
      fi
    fi
  fi
done
if $bool; then
  exit 1
else
  echo "No pylint errors found."
fi

exit 0
