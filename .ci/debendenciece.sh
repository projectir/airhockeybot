#!/bin/sh

set -e

#check installed debendenciece
echo "Checking software depencies..."

winenv="$(uname -s)"

if [ -f /etc/debian_version ] || [ -f /etc/arch-release ] || [ -z "${winenv##*CYGWIN*}" ] || [ -z "${winenv##*MINGW*}" ]; then

  #check shell file linter
  command -v shellcheck > /dev/null 2>&1 || ( echo "please install shellcheck"; exit 1 )
  #check yaml file linter
  command -v yamllint > /dev/null 2>&1 || ( echo "please install yamllint"; exit 1 )
  #check xml file linter
  command -v xmllint > /dev/null 2>&1 || ( echo "please install libxml2-utils"; exit 1 )
  #check python file linter
  command -v pylint > /dev/null 2>&1 || ( echo "please install python-pylint"; exit 1 )
else
  echo "Only compatible with Debian, Archlinux or Windows (with fixed POSIX sh)."
  exit 1
fi
exit 0
