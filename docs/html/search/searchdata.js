var indexSectionsWithContent =
{
  0: "_abcdefghilmoprstuvz",
  1: "abcfghimoprstz",
  2: "cdfmpsv",
  3: "acprt",
  4: "_abcdefgilmprstuv",
  5: "crs",
  6: "a",
  7: "bmopu",
  8: "f"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "defines",
  8: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Macros",
  8: "Modules"
};

