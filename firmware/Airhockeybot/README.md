# Firmware #
All software in use on the microcontroller

This module contains all software in use on the microcontroller.
in mind that the Arduino framework compiles all `*.ino` files into one single `*.cpp` file prior to executing,
so this split into multiple files is purely for readability and convenience.

There is the main loop and setup in [Airhockeybot.ino](Airhockeybot.ino).

Command parsing related parts like splitting the serial input and mapping prefix to command type is in the [CommandParsing.ino](CommandParsing.ino).

Anything related to motor control is in the [RobotControl.ino](RobotControl.ino).

Execution of commands is handled in [Commands.ino](Commands.ino).

Serial printing is handled in [ComponentSerialOut.ino](ComponentSerialOut.ino).

Configuration is done via variables defined in [Configuration.h](Configuration.h).

[Command.h](Command.h) and [Point.h](Point.h) define structs used for grouping properties together.

## Installation ##
To flash the firmware onto the used board, we use the Arduino framework.

First, install the required libraries..
Currently, it is only the AccelStepper library.
```bash
arduino-cli lib install Accelstepper
```
This is also coveniently stored in the [setup.sh](setup.sh).


After that, the firmware needs to be compiled and uploaded to the board.
To compile, use the Arduino-CLI and the correct board as a target.
```bash
arduino-cli compile --fqbn arduino:avr:mega Airhockeybot.ino
```
We are compiling for the Arduino Mega board, as its processor is the same as the microcontroller we use.
If you are using a different board, make sure to install the correct target board for the compiler, as well as update the pin assignment in [Configuration.h](Configuration.h).

Upload the compiled code onto the board by using the following command.
```bash
#Replace "$1" with the TTY port of your board
arduino-cli upload -p "$1" --fqbn arduino:avr:mega Airhockeybot.ino
```

The compiling and uploading procedure is stored in [compile.sh](compile.sh).

To automate this process, run [setup.sh](setup.sh) once, and [compile.sh](compile.sh) after every change.
