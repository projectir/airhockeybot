/**
 * @file
 * @ingroup firmware
 *
 * @brief Lower-level robot control, like speed configuration and target setting.
 * @details
 * This file contains mostly wrappers for functions from the AccelStepper library, as we delegated all motor control to it.
 **/

// absolute postions in mm. used for unit conversion
long x_abs_mm;
long y_abs_mm;

// (steps * microstepping) / (teeth * pitch) = steps/mm
float y_unit = 2.5;
float x_unit = 2.5;

AccelStepper stepper_y(AccelStepper::DRIVER, ORIG_Y_STEP_PIN, ORIG_Y_DIR_PIN);
AccelStepper stepper_x1(AccelStepper::DRIVER, ORIG_X1_STEP_PIN, ORIG_X1_DIR_PIN);
AccelStepper stepper_x2(AccelStepper::DRIVER, ORIG_X2_STEP_PIN, ORIG_X2_DIR_PIN);


/**
  *@brief Set the desired, constant speed for all motors in steps per second.
  *@param newSpeed desired constant speed in steps per second
  **/
void setSpeed(int newSpeed) {
  stepper_y.setSpeed(newSpeed);
  stepper_x1.setSpeed(newSpeed);
  //Inverted speed for the second linear guide on the X-axis.
  stepper_x2.setSpeed(-newSpeed);
}

/**
  *@brief Set the maximum allowed speed for all motors in steps per second.
  *@param max_speed maximal allowed speed in steps per second. Must be greater than 0.
  **/
void setMaxSpeed(unsigned int max_speed) {
  stepper_y.setMaxSpeed(max_speed);
  stepper_x1.setMaxSpeed(max_speed);
  stepper_x2.setMaxSpeed(max_speed);
}

/**
  *@brief Set the maximum allowed speed for X-Axis motors in steps per second.
  *@param max_speed maximal allowed speed in steps per second. Must be greater than 0.
  **/
void setMaxSpeedX(unsigned int max_speed) {
  stepper_x1.setMaxSpeed(max_speed);
  stepper_x2.setMaxSpeed(max_speed);
}

/**
  *@brief Set the maximum allowed speed for Y-Axis motors in steps per second.
  *@param max_speed maximal allowed speed in steps per second. Must be greater than 0.
  **/
void setMaxSpeedY(unsigned int max_speed) {
  stepper_y.setMaxSpeed(max_speed);
}

/**
  *@brief Set the acceleration rate both axis.
  *@param accelaration Desired accelaration in steps per second. Must be greater 0.
  **/
void setAcceleration(float accelaration) {
  stepper_y.setAcceleration(accelaration);
  stepper_x1.setAcceleration(accelaration);
  stepper_x2.setAcceleration(accelaration);
}

/**
  *@brief Set the acceleration rate for the X-Axis.
  *@param accelaration Desired accelaration in steps per second. Must be greater 0.
  **/
void setAccelerationXAxis(float accelaration) {
  stepper_x1.setAcceleration(accelaration);
  stepper_x2.setAcceleration(accelaration);
}

/**
  *@brief Set the acceleration rate for the Y-Axis.
  *@param accelaration Desired accelaration in steps per second. Must be greater 0.
  **/
void setAccelerationYAxis(float accelaration) {
  stepper_y.setAcceleration(accelaration);
}

// function using steps not mm
void moveToInternal(long x, long y) {
  stepper_y.moveTo(y);
  stepper_x1.moveTo(x);
  //Invert second X-axis guide
  stepper_x2.moveTo(-x);
}

/**
  *@brief Move to an absolute position.
  *@param x absolute X-position in millimeter.
  *@param y absolute Y-position in millimeter.
  **/
void moveTo(long x, long y) {
  //Store absolute positions
  x_abs_mm = x;
  y_abs_mm = y;

  //Convert to steps
  moveToInternal(x_abs_mm * x_unit, y_abs_mm * y_unit);
}

/**
  *@brief Move to a relative position.
  *@param x relative X-position in millimeter.
  *@param y relative Y-position in millimeter.
  **/
void move(long x, long y) {
  moveTo(x_abs_mm + x, y_abs_mm + y);
}

/**
  *@brief Get the speed along the X-axis.
  *@return Current set speed along the X-axis in millimeter.
  **/
float getXSpeed() {
  //Assume x1 and x2 are set to the same, but inverted values
  return (stepper_x1.speed() / x_unit);
}

/**
  *@brief Get the speed along the Y-axis.
  *@return Current set speed along the Y-axis in millimeter.
  **/
float getYSpeed() {
  return (stepper_y.speed() / y_unit);
}

/**
  *@brief Stop all motors.
  **/
void stop() {
  stepper_y.stop();
  stepper_x1.stop();
  stepper_x2.stop();
}

/**
  *@brief Get the current position in millimeter.
  *@return Point with x,y at the current position
  **/
Point getCurrentPos() {
  //Convert steps to millimeter
  int current_y_pos = stepper_y.currentPosition() / y_unit;
  int current_x_pos = stepper_x1.currentPosition() / x_unit;
  return Point{current_x_pos, current_y_pos};
}

/**
  *@brief Check if all motors are at their set target position.
  *@return bool whether all motors are at the set target position
  **/
bool allMotorsAtTargetPosition() {
  return atTargetPosition(stepper_x1) && atTargetPosition(stepper_x2) && atTargetPosition(stepper_y);
}

float prev_y_speed = 0.0;
float prev_x_speed = 0.0;
bool x_bypass_succ = false;
bool y_bypass_succ = false;

/**
  * TODO Tarek Sag Bescheid wenn fertig
  * @brief
  * @details
  **/
void continiousCommanding() {
  // Check M1 cammant is valid
  if (cQueue[0].action_type == Command::ACTIONS::MOVE) {
    if (cQueue[0].action_number == 1) {
      if (cQueue[0].parameter_count == 2) {

        float x_delta = 0.0;
        //get lowering x speed
        int current_x_speed = stepper_x1.speed();
        if (stepper_x1.speed() < 0) {
          current_x_speed = stepper_x1.speed() * (-1);
        }
        x_delta = prev_x_speed - current_x_speed;
        prev_x_speed = current_x_speed;
        if (x_delta < 0.0) {
          x_delta = 0.0;
        }

        float y_delta = 0.0;
        //get lowering y speed
        int current_y_speed = stepper_y.speed();
        if (stepper_y.speed() < 0) {
          current_y_speed = stepper_y.speed() * (-1);
        }
        y_delta = prev_y_speed - current_y_speed;
        prev_y_speed = current_y_speed;
        if (y_delta < 0.0) {
          y_delta = 0.0;
        }

        //Get delta distance which is left to target x
        int delta_x_current_distance = 0;
        if (stepper_x1.targetPosition() > stepper_x1.currentPosition()) {
          delta_x_current_distance = stepper_x1.targetPosition() - stepper_x1.currentPosition();
        } else {
          delta_x_current_distance = stepper_x1.currentPosition() - stepper_x1.targetPosition();
        }

        int delta_x_future_distance = 0;
        // Get delta distance which ist left from target x to second comand target x
        if (cQueue[0].params[0] > stepper_x1.targetPosition()) {
          delta_x_future_distance = cQueue[0].params[0] - stepper_x1.targetPosition();
        } else {
          delta_x_future_distance = stepper_x1.targetPosition() - cQueue[0].params[0];
        }

        //Get delta distance which is left to target y
        int delta_y_current_distance = 0;
        if (stepper_y.targetPosition() > stepper_y.currentPosition()) {
          delta_y_current_distance = stepper_y.targetPosition() - stepper_y.currentPosition();
        } else {
          delta_y_current_distance = stepper_y.currentPosition() - stepper_y.targetPosition();
        }

        int delta_y_future_distance = 0;
        // Get delta distance which ist left from target y to second comand target y
        if (cQueue[0].params[1] > stepper_y.targetPosition()) {
          delta_y_future_distance = cQueue[0].params[1] - stepper_y.targetPosition();
        } else {
          delta_y_future_distance = stepper_y.targetPosition() - cQueue[0].params[1];
        }

        //Only enter if x-axe deceleration hat begun.
        if (x_delta > 0.0) {
          if (delta_x_current_distance != 0 and delta_x_future_distance != 0) {
            // Check if the current delta greather than the future delta
            if (((delta_x_current_distance > delta_x_future_distance) and (stepper_x1.speed() > 0)) or ((delta_x_future_distance > delta_x_current_distance) and (stepper_x1.speed() < 0))) {
              // Bypase x ccordinates from future commant.
              x_abs_mm = cQueue[0].params[0];
              stepper_x1.moveTo(x_abs_mm * x_unit);
              stepper_x2.moveTo(x_abs_mm * x_unit * (-1));
              x_bypass_succ = true;
            }
            if (stepper_y.currentPosition() == cQueue[0].params[1]) {
              // Bypase y ccordinates from future commant.
              y_abs_mm = cQueue[0].params[1];
              stepper_y.moveTo(y_abs_mm * y_unit);
              y_bypass_succ = true;
            }
          }
          if (delta_x_current_distance != 0 and delta_y_future_distance != 0) {
            // Check if the current delta greather than the future delta
            if (((delta_x_current_distance <= delta_y_future_distance) and (stepper_y.speed() <= stepper_x1.speed())) or ((delta_x_current_distance < 0) and (delta_x_current_distance * (-1) <= delta_y_future_distance))) {
              y_abs_mm = cQueue[0].params[1];
              stepper_y.moveTo(y_abs_mm * y_unit);
              y_bypass_succ = true;
            }
          }
        }

        //Only enter if y-axe deceleration hat begun.
        if (y_delta > 0.0) {
          if (delta_y_current_distance != 0 and delta_y_future_distance != 0) {
            // Check if the current delta greather than the future delta
            if (((delta_y_current_distance > delta_y_future_distance) and (stepper_y.speed() > 0)) or ((delta_y_future_distance > delta_y_current_distance) and (stepper_y.speed() < 0))) {
              // Bypase y ccordinates from future commant.
              y_abs_mm = cQueue[0].params[1];
              stepper_y.moveTo(y_abs_mm * y_unit);
              y_bypass_succ = true;
            }
            if (stepper_x1.currentPosition() == cQueue[0].params[0]) {
              // Bypase x ccordinates from future commant.
              x_abs_mm = cQueue[0].params[0];
              stepper_x1.moveTo(x_abs_mm * x_unit);
              stepper_x2.moveTo(x_abs_mm * x_unit * (-1));
              x_bypass_succ = true;
            }
          }
          if (delta_y_current_distance != 0 and delta_x_future_distance != 0) {
            // Check if the current delta greather than the future delta
            if (((delta_y_current_distance <= delta_x_future_distance) and (stepper_x1.speed() <= stepper_y.speed())) or ((delta_y_current_distance < 0) and (delta_y_current_distance * (-1) <= delta_x_future_distance))) {
              x_abs_mm = cQueue[0].params[0];
              stepper_x1.moveTo(x_abs_mm * x_unit);
              stepper_x2.moveTo(x_abs_mm * x_unit * (-1));
              x_bypass_succ = true;
            }
          }
        }

        //shift next command if next command fully bypass.
        if (x_bypass_succ and y_bypass_succ) {
          x_bypass_succ = false;
          y_bypass_succ = false;
          shiftLeftCommandQueue();
        }
      }
    }
  }
}

/**
  *@brief Check if a motor is at its set target position
  *@return bool whether the motor is at its target position
  **/
bool atTargetPosition(AccelStepper motor) {
  return motor.targetPosition() == motor.currentPosition();
}

/**
  * @brief Count as long as an endstop is triggered.
  * TODO
  **/
void endstopTriggered(byte &counter, const byte port, AccelStepper stepper, const byte limit, const short move_val) {
  if (counter < limit) {
    counter++;
    if (!digitalRead(port)) {
      stepper.move(move_val);
      counter = 0;
    }
  }
  delayMicroseconds(2000); // delay the actor movemend.
  stepper.runToPosition();
}

/**
  *@brief Start the homing sequence.
  **/
void startHomingSequence() {

  byte treshholdY = 0;
  byte treshholdX1 = 0;
  byte treshholdX2 = 0;
  const byte limit = 3;

  // Homing the Y-axes
  while (!(treshholdY == limit)) {
    endstopTriggered(treshholdY, ORIG_Y_MIN_PIN, stepper_y, limit  , 1);
  }

  // Homing the X-axes
  while (!(treshholdX2 == 3 && treshholdX1 == 3)) {
    // x1
    endstopTriggered(treshholdX1, ORIG_X1_MIN_PIN, stepper_x1, limit, -1);

    // x2
    endstopTriggered(treshholdX2, ORIG_X2_MIN_PIN, stepper_x2, limit, 1);
  }

  // Set all motors current position
  // Y-axes is inverted, so it must be set to its max value
  stepper_y.setCurrentPosition(MAX_Y_PLAYING_FIELD * y_unit);
  stepper_x1.setCurrentPosition(0);
  stepper_x2.setCurrentPosition(0);
  moveTo(0, 0);
  Println(F("Init home done!"));
}

#ifdef DEBUG
void StartStepLossCounting() {

  byte treshholdY = 0;
  byte treshholdX1 = 0;
  byte treshholdX2 = 0;
  const byte limit = 3;

  while (!digitalRead(ORIG_Y_MIN_PIN)) {

    moveToInternal(stepper_x1.currentPosition(), stepper_y.currentPosition() +1);
    stepper_y.runToPosition();
  }

  while  ((!digitalRead(ORIG_X1_MIN_PIN)) || !(digitalRead(ORIG_X2_MIN_PIN))) {

    moveToInternal(stepper_x1.currentPosition() -1, stepper_y.currentPosition());
    stepper_x1.runToPosition();
    stepper_x2.runToPosition();
  }

	long y,x1,x2;
	y = stepper_y.currentPosition();
  y = y - MAX_Y_PLAYING_FIELD * y_unit;
	x1 = stepper_x1.currentPosition();
  x2 = stepper_x2.currentPosition();

  Println(F("Step loss counting finished"));
  Print(F("Lost steps  y: "));
  Println(String(y));
  Print(F("Lost steps x1: "));
  Println(String(x1));
  Print(F("Lost steps x2: "));
  Println(String(x2));
}
#endif

/**
  *@brief Invert the direction of the Y-axes.
  **/
void invertYAxis() {
  stepper_y.setPinsInverted(true);
}

/**
  *@brief Do one step with all motors.
  *@details This only steps the motors if there is a pending step.
  *Must be called as often as possible otherwise steps might be lost.
  **/
void stepAllMotors() {
  stepper_y.run();
  stepper_x1.run();
  stepper_x2.run();
}
