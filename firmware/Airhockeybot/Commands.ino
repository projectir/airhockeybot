/**
 * @file
 * @ingroup firmware
 *
 * @brief Functionality of commands, like how to execute, print, check if they finished.
 * @details
 * Based on the command type and the parameter count, each Command struct has to be handled differently.
 * This information, which would in other programmming languages be stored in subclasses of a common interface,
 * is stored here.
 * Currently, this means how to execute each command, how to print it and how to check if it finished running, is stored here.
 **/

/**
  *@brief Procedure for M1; Perform an absolute movement with a constant speed
  *@param x_pos Absolute movement on the X-axes
  *@param y_pos Absolute movement on the Y-axes
  *@param speed_ Constant speed in steps per second. Must be greater than 0
  **/
void commandM1(int x_pos, int y_pos, int speed_) {
  setSpeed(speed_);
  commandM1(x_pos, y_pos);
}

/**
  *@brief Procedure for M1; Perform an absolute movement
  *@param x_pos Absolute movement on the X-axes
  *@param y_pos Absolute movement on the Y-axes
  **/
void commandM1(int x_pos, int y_pos) {
  moveTo(x_pos, y_pos);
}

/**
  *@brief Procedure for M2; Perform a relative movement with a constant speed
  *@param x_pos Relative movement on the X-axes
  *@param y_pos Relative movement on the Y-axes
  *@param speed_ Constant speed in steps per second. Must be greater than 0
  **/
void commandM2(int x_pos, int y_pos, unsigned int speed_) {
  setSpeed(speed_);
  commandM2(x_pos, y_pos);
}

/**
  *@brief Procedure for M2; Perform a relative movement
  *@param x_pos Relative movement on the X-axes
  *@param y_pos Relative movement on the Y-axes
  **/
void commandM2(int x_pos, int y_pos) {
  move(x_pos, y_pos);
}

/**
  *@brief Procedure for M3; Perform the homing sequence
  **/
void commandM3() {
  startHomingSequence();
}

/**
  *@brief Procedure for M4; Perform an emergency stop
  **/
void commandM4() {
  stop();
}

#ifdef DEBUG
/**
 * M5; Initiates a modified homing sequence that is used to determine
 * the number of lost steps
 *
 * Usage:
 * 1. Any command sequence that might lose steps
 * 2. M5;
 *
 * If M5 makes contact with the endstops it prints out the location it
 * thinks it is at. 0 Would mean that no steps have been
 * lost. Negative/positive numbers indicate the direction of step
 * loss.  (true position is closer/father away from the endstop than
 * expected). For the axis with two motors the values for both motors might differ.
 *
 */
void commandM5() {
  StartStepLossCounting();
}
#endif

/**
  *@brief Procedure for I1; Print the current position
  **/
void commandI1() {
  char strbuff[50];
  Point currentPos = getCurrentPos();
  sprintf(strbuff, "I1 x:%f\ty:%f\tx_speed:%f\ty_speed:%f", currentPos.x, currentPos.y, getXSpeed(), getYSpeed());
  Println(strbuff);
}

/**
  *@brief Procedure for I2; Print the current command queue
  **/
void commandI2() {
  for (int i = 0; i < cQueue_fill; i++) {
    print(cQueue[i]);
  }
}

/**
  *@brief Procedure for C1; Set the max speed value in steps per second
  *@param max_speed speed in steps per second
  **/
void commandC1(unsigned int max_speed) {
  setMaxSpeed(max_speed);
}

/**
  *@brief Procedure for C2; Set the accelaration value
  *@param accelaration accelaration to set the motors to
  **/
void commandC2(float accelaration) {
  setAcceleration(accelaration);
}

/**
  *@brief Execute a given command
  *@param command Command to execute
  **/
void execute(Command command) {
  //Switch on first letter of prefix, then on the following number
  switch (command.action_type) {
    case Command::ACTIONS::MOVE:
      switch (command.action_number) {
        case 1:
          if (command.parameter_count == 3) {
            commandM1(command.params[0], command.params[1], command.params[2]);
          } else if (command.parameter_count == 2) {
            commandM1(command.params[0], command.params[1]);
          }
          break;
        case 2:
          if (command.parameter_count == 3) {
            commandM2(command.params[0], command.params[1], command.params[2]);
          } else if (command.parameter_count == 2) {
            commandM2(command.params[0], command.params[1]);
          }
          break;
        case 3:
          commandM3();
          break;
        case 4:
          commandM4();
          break;
#ifdef DEBUG
        case 5:
          commandM5();
          break;
#endif
        default:
          Println(F("Unknown M command"));
      }
      break;
    case Command::ACTIONS::CONFIG:
      switch (command.action_number) {
        case 1:
          if (command.parameter_count == 1) {
            commandC1(command.params[0]);
          }
          break;
        case 2:
          if (command.parameter_count == 1) {
            commandC2(command.params[0]);
          }
          break;
        default:
          Println(F("Unknown C command"));
      }
      break;
    case Command::ACTIONS::QUEUE_DROP:
      resetCQueue();
      break;
    case Command::ACTIONS::OVERRIDE:
      return;
    case Command::ACTIONS::INFO:
      switch (command.action_number) {
        case 1:
          commandI1();
          break;
        case 2:
          commandI2();
          return;
        default:
          Println(F("Unknown I command"));
      }

    case Command::ACTIONS::NULL_OP:
      return;

  }
}

/**
  *@brief Check if a command has finished
  *@details
  * All commands are reported as finished, beside ACTIONS::MOVE commands.
  * For these, the target position of each motor is compared to the current position.
  *@param command Command to check
  *@return bool if the command has finished
  **/
bool finished(Command command) {
  switch (command.action_type) {
    case Command::ACTIONS::CONFIG:
      return true;
    case Command::ACTIONS::NULL_OP:
      return true;
    case Command::ACTIONS::INFO:
      return true;
    case Command::ACTIONS::MOVE:
      continiousCommanding();
      return allMotorsAtTargetPosition() ;
    default:
      return false;
  }
}

/**
  *@brief Print a string representation of a command
  *@param command Command to represent
  **/
void print(Command command) {
  switch (command.action_type) {
    case Command::ACTIONS::MOVE:
      switch (command.action_number) {
        case 1:
          if (command.parameter_count == 3) {
            int x_pos = command.params[0];
            int y_pos = command.params[1];
            int speed_ = command.params[2];
            Println(F("M1 Command"));
            char strbuff[34];
            sprintf(strbuff, "\tSpeed: %i\tX: %i\tY: %i", speed_, x_pos, y_pos);
            Println(strbuff);
          } else if (command.parameter_count == 2) {
            int x_pos = command.params[0];
            int y_pos = command.params[1];
            Println(F("M1 Command"));
            char strbuff[20];
            sprintf(strbuff, "\tX: %i\tY: %i", x_pos, y_pos);
            Println(strbuff);
          }
          break;
        case 2:
          if (command.parameter_count == 3) {
            int x_pos = command.params[0];
            int y_pos = command.params[1];
            int speed_ = command.params[2];
            Println(F("M2 Command"));
            char strbuff[34];
            sprintf(strbuff, "\tSpeed: %i\tX: %i\tY: %i", speed_, x_pos, y_pos);
            Println(strbuff);
          } else if (command.parameter_count == 2) {
            int x_pos = command.params[0];
            int y_pos = command.params[1];
            Println(F("M2 Command"));
            char strbuff[20];
            sprintf(strbuff, "\tX: %i\tY: %i", x_pos, y_pos);
            Println(strbuff);
          }
          break;
        case 3:
          Println(F("M3 Command"));
          commandM3();
          break;
        case 4:
          Println(F("M4 Command"));
          commandM4();
          break;
#ifdef DEBUG
        case 5:
          Println("M5 Command");
          commandM5();
          break;
#endif
        default:
          Println(F("Unknown M command"));
      }
      break;
    case Command::ACTIONS::CONFIG:
      switch (command.action_number) {
        case 1:
          if (command.parameter_count == 1) {
            Println(F("C1 Command"));
          }
          break;
        case 2:
          if (command.parameter_count == 1) {
            Println(F("C2 Command"));
          }
          break;
        default:
          Println(F("Unknown C command"));
      }
      break;
    case Command::ACTIONS::INFO:
      switch (command.action_number) {
        case 1:
          Println(F("I1 Command"));
          break;
        case 2:
          Println(F("I2 Command"));
          return;
        default:
          Println(F("Unknown I command"));
      }

    case Command::ACTIONS::NULL_OP:
      return;

  }
}
