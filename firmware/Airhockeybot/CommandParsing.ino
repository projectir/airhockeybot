/**
 * @file
 * @ingroup firmware
 *
 * @brief  Parts related to command parsing, like string splitting or checking for terminated commands.
 * @details
 * Read serial input is delegated to this file in order to convert it to Command structs.
 * Multiple different helper functions are necessesary for that.
 * These are collected in this file.
 **/

/// Char used to terminate commands.
const char command_terminator = ';';

/**
  *@brief Check of a char array input is terminated with the command_terminator
  *@param input char array to check for command_terminator
  *@param input_size size if input
  *@return bool if command_terminator is at correct position in input
  **/
bool commandTerminated(char input[], unsigned int input_size) {
  if (input_size > 0) {
    return (input[input_size - 2] == command_terminator);
  }
  return false;
}

/**
@brief Parse input string readChars into seperate commands
@details
Input strings like "C1 P1,P2,P3&C2;" are parsed into seperate commands.
Commands are seperated by the '&' char and terminated by command_terminator.

The above example contains two commands, C1 with parameters P1,P2 and P3, and a parameterless C2 command.
Each command has a prefix, like C1, which determines the type of the command.
Followed by a space, and a comma-seperated list of parameters.

After the parameter list, the command can be terminated by the command_terminator or a new one can be started via the '&'.
Only the final command in a list connected by '&' has to be terminated by the command_terminator.
@param readChars input string to parse
@param output output array to store parsed commands in
@return number of commands found
**/
unsigned int parseCommand(String readChars, Command output[]) {
  String subcommands[MAX_COMMANDS];
  unsigned int subcommand_count = splitStringAt(readChars, "&", subcommands);
  for (int i = 0; i < subcommand_count; i++) {
    String command = subcommands[i];
    String action_type = command.substring(0, 1);
    String action_number = command.substring(1, 2);
    command.remove(0, 3);
    String parameter[MAX_PARAMS];
    unsigned int parameter_count = splitStringAt(command, ",", parameter);
    output[i] = generateCommand(action_type, action_number.toInt(), parameter, parameter_count);
  }
  return subcommand_count;
}

/**
  *@brief Split a string at a certain delimiter
  *@param s String to split
  *@param delimiter delimiter to split at
  *@param output array to store found substrings in
  *@return int count of found substrings
  **/
unsigned int splitStringAt(String s, String delimiter, String output[]) {
  s.trim();
  if (s.equals("")) return 0;
  unsigned int  substring_counter = 0;
  size_t pos = 0;
  size_t prev_pos = 0;
  while ((pos = s.indexOf(delimiter, prev_pos)) != -1) {
    String readChars = s.substring(prev_pos, pos);
    output[substring_counter++] = readChars;

    prev_pos = pos + 1;
  }
  String final_substring = s.substring(prev_pos);
  output[substring_counter++] = final_substring;
  return substring_counter;
}

/**
  *@brief Map command prefixes to command types
  *@param action Prefix string to map to command type
  **/
Command::ACTIONS resolveCommandAction(String action) {
  if ( action == F("M") ) return  Command::ACTIONS::MOVE;
  if ( action == F("C") ) return Command::ACTIONS::CONFIG;
  if ( action == F("I") ) return Command::ACTIONS::INFO;
  if ( action == F("Q") ) return Command::ACTIONS::QUEUE_DROP;
  if ( action == F("O") ) return Command::ACTIONS::OVERRIDE;

  Println(F("Unknown command"));
  return Command::ACTIONS::NULL_OP;
}

/**
  *@brief Build a command based on given substrings
  *@param action_type String specifying the type of command
  *@param action_number String further specifying the type of command
  *@param params String array storing string representation of the found parameter
  *@return Command generated from the input specifiers
  **/
Command generateCommand(String action_type, unsigned int action_number, String params[], unsigned int parameter_count) {
  Command command;
  command.action_type = resolveCommandAction(action_type);
  command.action_number = action_number;

  int int_parameter[MAX_PARAMS];

  for (int i = 0; i < parameter_count; i++) {
    int_parameter[i] = params[i].toInt();
  }

  memcpy(command.params, int_parameter, sizeof(int)* MAX_PARAMS);
  command.parameter_count = parameter_count;
  return command;
}
