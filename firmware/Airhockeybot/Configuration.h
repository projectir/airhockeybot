/**
 * @file
 * @ingroup firmware
 *
 * @brief Pin assignment
 **/

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)

#define ORIG_X1_STEP_PIN    26
#define ORIG_X1_DIR_PIN     27
#define ORIG_X1_ENABLE_PIN  25
#define ORIG_X1_MIN_PIN     47

#define ORIG_Y_STEP_PIN     32
#define ORIG_Y_DIR_PIN      33
#define ORIG_Y_ENABLE_PIN   31
#define ORIG_Y_MIN_PIN      45

#define ORIG_X2_STEP_PIN    40
#define ORIG_X2_DIR_PIN     41
#define ORIG_X2_ENABLE_PIN  37
#define ORIG_X2_MIN_PIN     46

#define LED_PIN             13

#elif defined(ARDUINO_AVR_UNO)

#define ORIG_X1_STEP_PIN    2
#define ORIG_X1_DIR_PIN     3
#define ORIG_X1_ENABLE_PIN  4
#define ORIG_X1_MIN_PIN     5

#define ORIG_Y_STEP_PIN     6
#define ORIG_Y_DIR_PIN      7
#define ORIG_Y_ENABLE_PIN   8
#define ORIG_Y_MIN_PIN      9

#define ORIG_X2_STEP_PIN    10
#define ORIG_X2_DIR_PIN     11
#define ORIG_X2_ENABLE_PIN  12
#define ORIG_X2_MIN_PIN     A0

#define LED_PIN             13

#endif

/*
   Enable Debugging features
*/
// #define DEBUG

/**
 * Flag for asyncronous printing
 **/
//#define USE_ASYNC_PRINT

/**
 * Used baudrate
 **/
#define BAUDRATE 115200

/**
 * Buffer size for serial incoming
 **/
#define PACKET_SIZE 64

/**
 * Buffer size for serial outgoing
 **/
#define OUT_BUFFER_SIZE 64

/**
 * Allowed number of Commands in queue
 **/
#define MAX_COMMANDS 5

/**
 * Maximal number of parameter for a single command
 **/
#define MAX_PARAMS 3

/**
 * Upper end of Y-axes in millimeter
 **/
#define MAX_Y_PLAYING_FIELD 815
