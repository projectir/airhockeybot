/**
 * @file
 * @ingroup firmware
 *
 * @brief Collection of test code written during the project.
 * @details
 * During our work on the firmware, some testing code was written.
 * It is collected in this file.
 * Note, that this is not an exhaustive list, most test code was deleted after fixing the corresponding bugs.
 **/


/**
  *@brief Continously bounce the carriage on the X-axis.
  **/
void loopTestBounceRunX() {
  int const waypoint_a = 1700;
  int const waypoint_b = 200;

  if ((stepper_x1.distanceToGo() == 0) && (stepper_x2.distanceToGo() == 0))
    switch (stepper_x1.currentPosition()) {
      case waypoint_a:
        stepper_x1.moveTo(waypoint_b);
        stepper_x2.moveTo(-waypoint_b);
        break;
      case waypoint_b:
        stepper_x1.moveTo(waypoint_a);
        stepper_x2.moveTo(-waypoint_a);
        break;
      default:

        stepper_x1.moveTo(waypoint_a);
        stepper_x2.moveTo(-waypoint_a);
        break;
    }
  stepper_x1.run();
  stepper_x2.run();
}

/**
  *@brief Continously bounce the carriage on the Y-axis.
  **/
void loopTestBounceRunY() {
  int const waypoint_a = 1700;
  int const waypoint_b = 200;

  if (stepper_y.distanceToGo() == 0)
    switch (stepper_y.currentPosition()) {
      case waypoint_a:
        stepper_y.moveTo(waypoint_b);
        break;
      case waypoint_b:
        stepper_y.moveTo(waypoint_a);
        break;
      default:
        stepper_y.moveTo(waypoint_a);
        break;
    }
  stepper_y.run();
}
