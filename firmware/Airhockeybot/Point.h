/**
 * @file
 * @ingroup firmware
 *
 * @brief Type definition of Point struct.
 **/

/**
 *@brief Store a point with a given X-,Y-Position.
 **/
struct Point {
  float x;
  float y;
};

typedef struct Point Point;
