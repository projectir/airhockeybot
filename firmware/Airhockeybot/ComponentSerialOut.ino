/**
 * @file
 * @ingroup firmware
 *
 * @brief Asynchronous printing attempt.
 * @details
 * In testing, we found that using the normal print() from the Arduino framework was limiting the motors and increasing step loss.
 * So, we tried implementing some sort of asynchronous printing functionality.
 **/

/**
  *@brief Print a String to serial console, terminated with a new line.
  *@param s String to print
  **/
void Println(const String s) {
#ifndef USE_ASYNC_PRINT
  Serial.println(s);
#else
  asyncPrintln(s);
#endif
}

//No camel case renaming as it would conflict with method print() in Commands.ino
/**
  *@brief Print a String to serial console.
  *@param s String to print
  **/
void Print(const String s) {
#ifndef USE_ASYNC_PRINT
  Serial.print(s);
#else
  asyncPrint(s);
#endif
}

#ifdef USE_ASYNC_PRINT

/**
  * TODO Mirko
  **/
uint16_t outBufferFill;
byte outBuffer[OUT_BUFFER_SIZE];

void outBufferClear() {
  memset(&outBuffer, 0, sizeof(outBuffer));
  outBufferFill = 0;
}

bool outBufferRShift(const uint16_t shift) {
  int16_t newFill = outBufferFill - shift;
  if (newFill > -1) {
    outBufferFill = newFill;
    memmove(outBuffer + shift, outBuffer, (sizeof(outBuffer) * sizeof(byte)));
    return 1;
  }
  return 0;
}

void asyncPrintln(const String s) {
  asyncPrint(s + "\n");
}

uint16_t asyncPrint(const String s) {
  uint16_t nBytes = s.length() + 1;

  if ((outBufferFill + nBytes) < OUT_BUFFER_SIZE) {
    byte *buf = &(outBuffer[outBufferFill]);
    s.getBytes(buf, s.length());

    outBufferFill += nBytes;

    return nBytes;
  }
  return 0;
}

uint16_t outBufferFlush() {
  const uint16_t avail = Serial.availableForWrite();

  const uint16_t written = min(avail, outBufferFill);

  if (written > 0) {
    Serial.write(outBuffer, written);
    outBufferRShift(written);
  }
  return written;
}
#endif
