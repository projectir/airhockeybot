/**
 * @file
 * @ingroup firmware
 *
 * @brief Type definition of the Command struct.
 **/

/**
 * @brief Store information related to a single command
 **/
struct Command {
  ///All possible command types
  enum class ACTIONS { NULL_OP = 0, MOVE, INFO, CONFIG, QUEUE_DROP , OVERRIDE} action_type;
  byte action_number;
  int params[MAX_PARAMS];
  byte parameter_count;
};

typedef struct Command Command;
