/**
  * @file
  * @ingroup firmware
  *
  * @brief Setup, main loop, and command queue.
  * @details
  * This file serves as the first, and main entry point for our firmware.
  * Serial input is read and fed into the command queue.
  * The queue is managed in this file.
  **/

#include "Configuration.h"
#include "Command.h"
#include "Point.h"

// Libaries installed with the arduino library manager
#include <AccelStepper.h>

// -1 because we want to have space for a NULL terminator
#define MAX_BUFFER_FILL (sizeof(SBuffer)-1)

/// Input buffer
char SBuffer[PACKET_SIZE + 4];
/// Input buffer fill
uint32_t SBuffer_fill;

///Command queue
Command cQueue[MAX_COMMANDS];
/// Currently running command
Command currentCommand;
/// Storing if a command is currently executing
boolean runningCommand = false;
/// Number of commands in queue
uint32_t cQueue_fill;

/**
  *@brief Overwrite the input buffer and reset the fill counter
  **/
void resetInputBuffer() {
  memset(&SBuffer, '\0', (sizeof(char)) * (sizeof SBuffer));
  SBuffer_fill = 0;
}

/**
  *@brief Copy all elements of the command queue one place down, dropping the first command.
  **/
void shiftLeftCommandQueue() {
  if (cQueue_fill == 0) return;
  memcpy(&cQueue, &cQueue[1], sizeof(cQueue) - sizeof(Command));
  cQueue_fill--;
}

/**
  *@brief Check if the queue is full.
  **/
bool commandQueueFull() {
  return cQueue_fill >= MAX_COMMANDS;
}

/**
  * @brief Empty the command queue an reset the fill counter.
  **/
void resetCQueue() {
  memset(&cQueue, 0, sizeof cQueue);
  cQueue_fill = 0;
}

/**
  *@brief Input commands into the queue.
  *@details
  *Parsed commands are input into the command queue.
  *If the queue is full, then the first one is dropped and the new one put at the last position.
  *If the new command is of type OVERRIDE, then the queue is dropped.
  *If the following command is not a movement command, then the motors are stopping.
  *
  *@param commands Commands to input into the queue
  *@param command_count Number of commands to input into the queue
  **/
void processCommands(Command commands[], unsigned int command_count) {
  for (int i = 0; i < command_count; i++) {
    Command command = commands[i];
    if (command.action_type == Command::ACTIONS::OVERRIDE)
    {
      //Override command
      resetCQueue();
      runningCommand = false;

      //If no movement command follows, stop the motors
      if ((i < (command_count - 1)) && (commands[i + 1].action_type != Command::ACTIONS::MOVE)) {
        stop();
      }
    }
    else {
      //Normal command
      if (commandQueueFull()) {
        shiftLeftCommandQueue();
      }
      //Input command at last place into the queue
      cQueue[cQueue_fill] = command;
      cQueue_fill++;
    }
  }
}

/**
  *@brief Upon incoming serial traffic, read it into the input buffer
  **/
void serialEvent() {
  if (Serial.available() > 0 && SBuffer_fill < (sizeof SBuffer)) {
    SBuffer[SBuffer_fill++] = Serial.read();
  }
}

/**
  *@brief Get the first command in queue and execute it
  **/
void getAndExecuteNextCommand() {
  currentCommand = cQueue[0];
  shiftLeftCommandQueue();
  execute(currentCommand);
  runningCommand = true;
}

/**
  *@brief Setup the airhockeybot
  **/
void setup() {
  // Stepper motor configuration
  setMaxSpeedX(3000);
  setAccelerationXAxis(2500);

  setMaxSpeedY(4000);
  setAccelerationYAxis(2500);

  // Endstop pins
  pinMode(ORIG_X1_MIN_PIN, INPUT_PULLUP);
  pinMode(ORIG_X2_MIN_PIN, INPUT_PULLUP);
  pinMode(ORIG_Y_MIN_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);  // Feedback LED

  // Communication
  Serial.begin(BAUDRATE);

  //Incoming
  resetInputBuffer();

#ifdef USE_ASYNC_PRINT
  //Outgoing
  outBufferClear();
#endif

  delay(1000);

  invertYAxis();
  //startHomingSequence();
}

/**
  *@brief main loop
  **/
void loop() {
// Check if input in serial buffer is a terminated command
  if (commandTerminated(SBuffer, SBuffer_fill)) {
    if (commandQueueFull()) {
      Println(F("Command queue full"));
    }

    //Parse commands an possible subcommands from input buffer
    Command commands[MAX_COMMANDS];
    unsigned int command_count = parseCommand(String(SBuffer), commands);
    Println(String(command_count));
    Println(F("commands parsed"));
    //Put parsed commands into queue
    processCommands(commands, command_count);
    resetInputBuffer();
  }

  //If running a command, check if it finished and there is a next command available.
  if (runningCommand) {
    if (finished(currentCommand)) {
      Println(F("Command finished"));
      if (cQueue_fill > 0) {
        getAndExecuteNextCommand();
      }
      else {
        runningCommand = false;
      }
    }
  }
  //If no command is running, check if there is one available
  else {
    if (cQueue_fill > 0) {
      getAndExecuteNextCommand();
    }
    else {
      runningCommand = false;
    }
  }

  stepAllMotors();
#ifdef USE_ASYNC_PRINT
  //Output part of the out buffer if async is set
  outBufferFlush();
#endif
}
