#!/bin/bash

upload() {
    echo "$1"
    arduino-cli upload -p "$1" --fqbn arduino:avr:mega Airhockeybot.ino
}

arduino-cli compile --fqbn arduino:avr:mega Airhockeybot.ino

TTYDEV=$(find  /dev -iname 'ttyACM*')
find  /dev -iname 'ttyACM*'
echo "${}"

case $TTYDEV in
    /dev/ttyACM* ) upload "$TTYDEV";;
    * ) echo "No ttyACM* found";;
esac
