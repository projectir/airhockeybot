
# Partlist from roboter-bausatz.de

1 x 10x Kugellager F688ZZ mit Flansch 8x16x5mm (RBS12844) - 7,64 EUR CAD

1 x 10 x Kugellager 625.ZZ 5*16*5 mm (RBS10472) - 2,95 EUR CAD

8 x GT2 Riemenscheibe Zahnrad 5mm Bohrung 40 Zähne 6mm Zahnriemen (RBS12899) - 2,65 EUR CAD

1 x 10 Meter GT2 Zahnriemen offen 6mm (RBS10449) - 13,52 EUR CAD

5 x Mechanischer Endschalter CMV101D (RBS10767) - 0,88 EUR

3 x ACT Nema 17 Schrittmotor 17HS5425 3.1V 2.5A 48mm (RBS12806) - 13,95 EUR

1 x MKS Robin E3 3D-Drucker ARM 32Bit Mainboard mit integrierten TMC2209 (RBS13800) - 43,94 EUR

25 x Kühlkörper für DRV8825/A4988/A4983/TMC2100 Schrittmotortreiber (RBS11834) - 0,33 EUR

1 x Netzteil 12V 20A 240W für 3D-Drucker / LED-Technik (RBS10726) - 16,95 EUR

2 x 1 Meter Schleppkette zur Kabelführung 7x7mm (RBS10756) - 4,95 EUR

10 x Schrittmotorkabel 4Pin auf 6 Pin HX2.54 150cm (RBS12886) - 1,65 EUR

2 x 12V 6015 Lüfter 60x60x15mm 2Pin (RBS12883) - 2,45 EUR

1 x Sunon Axiallüfter 60x60x15mm 24V MF60152V21000UA99 (RBS13398) - 5,15 EUR

4 x GT2 Riemenscheibe Zahnrad 8mm Bohrung 40 Zähne 6mm Zahnriemen (RBS12900) - 2,45 EUR CAD

5 x Spannungswandler LM2596 DC-DC Step-Down Modul 2A (RBS10032) - 1,25 EUR

# Partlist from igus.de

1 x drylin W Doppelschiene WS [link](https://www.igus.de/product/732), WS-10-40-ungebohrt, 40mm,  1100mm, 35,78 EUR, see ![alt text](https://www.igus.de/contentData/Products/ProductPictures/p732i25521_89x59.jpg) CAD

1 x drylin W Komplettschlitten WW [link](https://www.igus.de/product/933), WW-10-40-10, 73mm, 100mm, 27,78 EUR CAD

2 x drylin R Edelstahlwelle, EEWM, 1.4034, [link](https://www.igus.de/product/144), EEWM-12, 12mm, 1100mm, 27,08 EUR CAD

5 x drylin R Lineargleitbuchse RJUM-01, [link](https://www.igus.de/product/1185), 12mm, 22mm, 32mm, 7,66 EUR CAD