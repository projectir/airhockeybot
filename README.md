# AirhockeyBot

This is the central repository for our air hockey robot, called **AirhockeyBot**, developed in the Masterproject Intelligent Robotics at the University of Hamburg SS20+WS20/21.

Overview            |  [Gazebo Simulation](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_simulation)
:-------------------------:|:-------------------------:
![](./docs/img/overview.jpeg) | ![](./docs/img/sim_seite.png)

[Mechanical Design](https://gitlab.com/projectir/airhockeybot/-/tree/master/mechanical_design)             |  [Firmware](https://gitlab.com/projectir/airhockeybot/-/tree/master/firmware/Airhockeybot) & [Controller](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_controller) 
:-------------------------:|:-------------------------:
![](./docs/img/ab_mech_mirrored.png)  |  ![](./docs/img/microstepping_bypass_new.jpg)

[Puck Detection](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_vision)             |  [Trajectory Estimation](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_trajectory) & [Action Generation](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_action)
:-------------------------:|:-------------------------:
![](./docs/img/detection.jpg)  |  ![](./docs/img/trajectory_estimate.jpg)


## Installation
0. Run `$ sudo apt-get update && sudo apt-get upgrade`

1. Install `python 3.6` and configure as default python interpreter: \
  run `$ sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.6 10`\
  Check `$ python -V`, it should tell you: `python3.6` \
  Install `gcc7` and configure as default compiler: \
  run `$ sudo apt install gcc-7 g++-7` and
  `$ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 90 --slave /usr/bin/g++ g++ /usr/bin/g++-7 --slave /usr/bin/gcov gcov /usr/bin/gcov-7`

2. Install `ROS`. We used `Ubuntu 18.04 LTS` in combination with `ROS Melodic`.
__Follow:__ http://wiki.ros.org/melodic/Installation/Ubuntu
__Install rosdep to python3__ (for sudo rosdep init): `python -m pip install rosdep`

3. Switch to your project directory:
   `cd <your_path_to_project_repo>/airhockeybot`

4. __Execute:__
```bash
python -m pip install --upgrade pip

python -m pip install -r requirements.txt  (Installs python dependencies)

sudo apt-get install v4l-utils          (Installs video capture API v4l2)

sudo apt-get install python-catkin-tools
```

5. Try `locate libpython3.6m.so` and check if it exists in your environment. If you can't determine it's location,
try leaving out the `-D PYTHON_LIBRARY=<path_to_python3>/lib/libpython3.6m.so` option in the command below

    __Execute:__
    ```bash
    catkin config \
    -DPYTHON_EXECUTABLE=<path_to_python3>/bin/python3 \
    -DPYTHON_INCLUDE_DIR=<path_to_python3>/include/python3.6m \
    -DPYTHON_LIBRARY=<path_to_python3>/lib/libpython3.6m.so \
    --no-install
    ```

    e.g.

    ```bash
        catkin config \
        -DPYTHON_EXECUTABLE=/usr/bin/python3 \
        -DPYTHON_INCLUDE_DIR=/usr/include/python3.6m \
        -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.6m.so \
        --no-install
    ```

6. __Execute:__ `catkin build` \
  If building OpenCV from source run: \
  `$ catkin build --cmake-args -DOPENCV_CUSTOM_BUILD=True`

7. __Execute:__ `source devel/setup.bash` (in case terminal is at content root) or
`source <your_path_to_project_repo>/airhockeybot/devel/setup.bash` (for absolute path)

8. __Execute:__ `roslaunch airhockeybot airhockeybot.launch`

### Build OpenCV 4.2.0
In our experiments, building OpenCV from source descreased inference time by 25%-70%. We recommend to use a custom built OpenCV (+ Cuda Support) in combination with a real hardware of AirhockeyBot.
It can be done e.g. according to [https://gist.github.com/raulqf/f42c718a658cddc16f9df07ecc627be7).
A custom `cmake` command with easier localization of required paths is:
```bash
cmake -D CMAKE_BUILD_TYPE=RELEASE \
-D CMAKE_C_COMPILER=$(which gcc-7) \
-D BUILD_opencv_python2=OFF \
-D CMAKE_INSTALL_PREFIX=$(python3 -c 'import sys; print(sys.prefix)') \
-D PYTHON3_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.6m.so \
-D PYTHON3_INCLUDE_DIR=$(python3 -c 'from distutils.sysconfig import get_python_inc; print(get_python_inc())') \
-D PYTHON3_EXECUTABLE=$(which python3) \
-D PYTHON3_PACKAGES_PATH=$(python3 -c 'from distutils.sysconfig import get_python_lib; print(get_python_lib())') \
-D BUILD_opencv_python2=OFF \
-D BUILD_NEW_PYTHON_SUPPORT=ON \
-D BUILD_opencv_python3=ON \
-D HAVE_opencv_python3=ON \
-D PYTHON_DEFAULT_EXECUTABLE=$(which python3) \
-D INSTALL_PYTHON_EXAMPLES=ON    \
-D INSTALL_C_EXAMPLES=OFF    \
-D OPENCV_ENABLE_NONFREE=ON    \
-D BUILD_EXAMPLES=ON \
-D WITH_GSTREAMER=OFF \
-D OPENCV_GENERATE_PKGCONFIG=ON ..
```

Note that only for `PYTHON3_LIBRARY` you still need to specify a path.

## Usage
We preconfigured the [run.sh](https://gitlab.com/projectir/airhockeybot/-/blob/master/run.sh) bash script to simplify the usage of airhockeybot. All 3 configuration options must be set to either `on` or `off`.
```bash
  bash run.sh <REBUILD> <CUSTOM_CV> <STATISTICS>
```

**`<REBUILD> [on|off]:`** Set to `òn` in case you want to rebuild the catkin workspace.

**`<CUSTOM_CV> [on|off]:`** Set to `òn` in case you want to rebuild (REBUILD=on) the catkin workspace and you have your __OpenCV built from source__.

**`<STATISTICS> [on|off]:`** Set to `òn` in case you want to start an instance of [rqt_graph](http://wiki.ros.org/rqt_graph).

To simply start AirhockeyBot up, execute the script with all options set to `off`:
```bash
  bash run.sh off off off
```
Which is equal to using the launchfile:
```bash
  roslaunch airhockeybot airhockeybot.launch
```


## Play with remote control
see [airhockeybot_simulation README](https://gitlab.com/projectir/airhockeybot/-/tree/master/src/airhockeybot_simulation)

## Overview of the ROS Architecture
![Communication Diagram](./docs/img/rosgraph.svg)

## Vision
### Video Capture
1) Show all video capture devices `v4l2-ctl --list-devices` and show the available controls `v4l2-ctl -d /dev/video0 --list-ctrls`

2) Make settings specific to our setup
```bash
v4l2-ctl --set-ctrl=exposure_auto=1
v4l2-ctl --set-ctrl=gain=255
v4l2-ctl --set-ctrl=exposure_absolute=20
v4l2-ctl --set-ctrl=focus_auto=0
v4l2-ctl --set-ctrl=focus_absolute=10
```

3) And capture videos with 60 FPS and MJPEG:
`ffmpeg -f video4linux2 -video_size 1920x1080 -framerate 60 -input_format mjpeg -i /dev/video0 -video_size 1920x1080 -filter:v fps=fps=60 -vcodec mjpeg -q:v 2 /homeL/project2020/Videos/airhockeybot/outputs.avi`

4) Finally convert avi videos to image set:
`ffmpeg -i /homeL/project2020/Videos/airhockeybot/outputs.avi -q:v 2 /homeL/project2020/Videos/airhockeybot/images_%04d.jpg`

### Dataset
All the steps above have already been done for you, that is we recorded a dataset of airhockey gameplay.
You can download the full dataset [here](https://unihamburgde-my.sharepoint.com/:f:/g/personal/marcus_rottschaefer_studium_uni-hamburg_de/EkoCk2EeFBlImc4YA0eZxMYBNBc48itYR66XH2LQTSvtzA?e=thzO8E).
To access the folder, you need to login with your `firstname.lastname@studium.uni-hamburg.de` email address.

The 12 videos of gameplay against the robot in different lightning conditions are available in 1080p, 720p, 480p and 360p. In total, it is about 10GB.

## Linked Resources
**Doxygen Documentation:** https://projectir.gitlab.io/airhockeybot/

**Global Constants:** [globals.json](globals.json)
> Physical objects global properties as constants, like size of the airhockey playing field and puck size. The units are centimeters (mm), grams (g).

**Publications:** See the [pdf section of our docs](https://gitlab.com/projectir/airhockeybot/-/tree/master/docs/pdf) for the theoretical background of this repository.

**Link to Mafiasi Etherpad:** [https://pad.mainframe.io/p/private_prir](https://pad.mainframe.io/p/private_prir)		*(deprecated; use git for documentation)*

## Development

## Git Repo configuration

This mechanism will help you to find potential errors and give you suggstions for possebile better solutions for some programlanguages.

### Preparing for Windows

Installing shellcheck:
 1. Download [shellcheck](https://storage.googleapis.com/shellcheck/shellcheck-latest.zip)
 2. Rename the *.exe file to shellcheck.exe
 3. Copy the *.exe into program directory into a folder prir
 4. Right click My Computer -> Click Properties -> Click Advanced system settings -> Click Environment Variables
 5. Add the Path to the prir directory

Installing yamllint:
 1. Follow this instruction to [install python and pip](https://github.com/BurntSushi/nfldb/wiki/Python-&-pip-Windows-installation)
 2. You may add the python*/Scripts folder to the Path ENV
 3. pip install yamllint

Installing xmllint:
 1. Download iconv, libxml2, libxmlsec , zlib from ftp://ftp.zlatkovic.com/libxml/ (for 64Bit choose files from 64bit/ directory)
 2. Extracte all zip files and copy the content of each directory into program directory into the folder prir
 3. Right click My Computer -> Click Properties -> Click Advanced system settings -> Click Environment Variables
 4. Add the Path to the prir\bin directory

Please add this hook to your local git installation by symlinking it:

### Enable git hook
```bash
ln -s ../../.ci/pre-push-hook .git/hooks/pre-push
```
